using System;

public class Example : Efl.Csharp.Application
{
    // Callback to quit the application
    public static void QuitCb(object sender, Efl.Gfx.EntityVisibilityChangedEventArgs e)
    {
        // Exit the EFL main loop
        if (e.Arg == false)
          Efl.App.AppMain.Quit(0);
    }

    protected override void OnInitialize(string[] args)
    {
        // Create a window and initialize it
        Efl.Ui.Win win = new Efl.Ui.Win(Efl.App.AppMain);
        // Set the window's title
        win.Text = "Hello World";
        // Request that the window is automatically hidden when the "close"
        // button is pressed
        win.Autohide = true;
        // Window size must be explicitly set, otherwise it will be invisible
        // due to its lack of content.
        win.Size = new Eina.Size2D(360, 240);
        win.VisibilityChangedEvent += QuitCb;
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch();
    }
}

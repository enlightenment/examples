using System;

public class Example : Efl.Csharp.Application
{
    // Callback to quit the application
    public static void QuitCb(object sender, Efl.Gfx.EntityVisibilityChangedEventArgs e)
    {
        // Exit the EFL main loop
        if (e.Arg == false)
          Efl.App.AppMain.Quit(0);
    }

    protected override void OnInitialize(string[] args)
    {
        // Create a window and initialize it
        Efl.Ui.Win win = new Efl.Ui.Win(Efl.App.AppMain);
        // Set the window's title
        win.Text = "Hello World";
        // Request that the window is automatically hidden when the "close"
        // button is pressed
        win.Autohide = true;
        // Hook to the Hide event
        win.VisibilityChangedEvent += QuitCb;

        // Create a box container
        var box = new Efl.Ui.Box(win);
        // Set its minimum size
        box.HintSizeMin = new Eina.Size2D(360, 240);
        // Set the box as the content for the window
        // The window size will adapt to the box size
        win.SetContent(box);

        // Create a text label widget
        var label = new Efl.Ui.Textbox(box);
        // Set its content and customize it
        label.Text = "Hello World. This is an Efl.Ui application!";
        label.Editable = false;
        label.SelectionAllowed = false;
        label.HintWeight = (1.0, 0.9);
        label.HintFill = (false, false);
        label.HintAlign = ((Efl.Gfx.Align)0.5, (Efl.Gfx.Align)0.5);
        // Add the text to the box container
        box.Pack(label);

        // Create a button widget
        var button = new Efl.Ui.Button(box);
        // Customize it
        button.Text = "Quit";
        button.HintWeight = (1.0, 0.1);
        // Set the method to be called when the button is pressed
        button.ClickedEvent += (object sender, Efl.Input.ClickableClickedEventArgs e) => { Efl.App.AppMain.Quit(0); };
        // Add the button to the box container
        box.Pack(button);
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch();
    }
}


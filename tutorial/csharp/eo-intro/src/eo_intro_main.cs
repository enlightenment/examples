using System;

public class Example : Efl.Csharp.Application
{
    static Efl.GenericModel root, child2;

    // Create our test hierarchy
    static void ObjCreate()
    {
        // First create a root element
        root = new Efl.GenericModel(null);
        root.Name = "Root";

        // Create the first child element
        var child = new Efl.GenericModel(root);
        child.Name = "Child1";

        // Create the second child element, this time, with an extra reference
        child2 = new Efl.GenericModel(root);
        child2.Name = "Child2";
    }

    // Destroy the test hierarchy
    static void ObjDestroy()
    {
        // Destroy the root element
        Console.WriteLine ("Deleting Root...\n");
        root.Dispose();

        // Destroy the child2 element, for which we were keeping an extra reference
        Console.WriteLine ("Deleting Child2...\n");
        child2.Dispose();
    }

    protected override void OnInitialize(string[] args)
    {
        // Create all objects
        ObjCreate();

        // Destroy all objects
        ObjDestroy();

        Efl.App.AppMain.Quit(0);
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch(Efl.Csharp.Components.Basic);
    }
}

#define EFL_BETA_API_SUPPORT 1

#include <Eina.h>
#include <Efl_Core.h>

/*
 * This helper method triggers lifecycle events for the purpose of this demo.
 * efl_pause and efl_resume may never be called for your application, depending
 * on your environment, therefore this demo triggers them directly to show how
 * you can respond.
 */

static void
_lifecycle_simulation(void *data, const Efl_Event *ev EINA_UNUSED)
{
   Efl_Loop *loop = data;
   static int called = 0;

   switch (called)
     {
        case 0:
          // First call, pause the application
          efl_event_callback_call(loop, EFL_APP_EVENT_PAUSE, NULL);
          break;
        case 1:
          // Second call, resume the application
          efl_event_callback_call(loop, EFL_APP_EVENT_RESUME, NULL);
          break;
        default:
          // Last call, exit the application
          efl_exit(0);
     }

   called++;
}

EAPI_MAIN void
efl_pause(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   printf("Lifecycle: paused\n");
}

EAPI_MAIN void
efl_resume(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   printf("Lifecycle: resumed\n");
}

EAPI_MAIN void
efl_terminate(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   printf("Lifecycle: terminated\n");
}

EAPI_MAIN void
efl_main(void *data EINA_UNUSED, const Efl_Event *ev)
{
   printf("Lifecycle: launched\n");

   // The timer function will trigger the chain of simulated events to show
   // how an app could respond to system lifecycle events.
   efl_add(EFL_LOOP_TIMER_CLASS, ev->object,
           efl_event_callback_add(efl_added, EFL_LOOP_TIMER_EVENT_TIMER_TICK, _lifecycle_simulation, ev->object),
           efl_loop_timer_interval_set(efl_added, 1.0));
}
EFL_MAIN_EX()


#define EFL_BETA_API_SUPPORT 1

#include <Eina.h>
#include <Elementary.h>
#include <Efl_Ui.h>

static void
_gui_quit_clicked_cb(void *data EINA_UNUSED, const Efl_Event *event EINA_UNUSED)
{
   efl_exit(0);
}

static void
_gui_setup()
{
   Eo *win;

   win = efl_add(EFL_UI_WIN_CLASS, efl_main_loop_get(),
                 efl_ui_win_type_set(efl_added, EFL_UI_WIN_TYPE_BASIC),
                 efl_ui_win_name_set(efl_added, "Lifecycle Example"),
                 efl_text_set(efl_added, "Hello World"),
                 efl_ui_win_autodel_set(efl_added, EINA_TRUE));
   efl_add(EFL_UI_BUTTON_CLASS, win,
           efl_text_set(efl_added, "Quit"),
           efl_content_set(win, efl_added),
           efl_gfx_hint_size_min_set(efl_added, EINA_SIZE2D(360, 240)),
           efl_event_callback_add(efl_added, EFL_INPUT_EVENT_CLICKED,
                                  _gui_quit_clicked_cb, efl_added));
}

EAPI_MAIN void
efl_pause(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   printf("Lifecycle: paused\n");
}

EAPI_MAIN void
efl_resume(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   printf("Lifecycle: resumed\n");
}

EAPI_MAIN void
efl_terminate(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   printf("Lifecycle: terminated\n");
}

EAPI_MAIN void
efl_main(void *data EINA_UNUSED, const Efl_Event *ev)
{
   printf("Lifecycle: launched\n");

   _gui_setup();
}
EFL_MAIN_EX()


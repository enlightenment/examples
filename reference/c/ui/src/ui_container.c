#define EFL_BETA_API_SUPPORT 1

#include <Elementary.h>
#include <Efl_Ui.h>

/*
 * Efl.UI container examples.
 *
 * Load and pack a selection of containers.
 * Each has it's own unique layout and methods which are demonstrated below.
 */

/*
 * TODO Layout
 * TODO - still ELM Conformant
 * TODO - still ELM Mapbuf
 * TODO - still ELM Scroller
 */

// quit the app, called if the user clicks the Quit button or the window is deleted
static void
_gui_quit_cb(void *data EINA_UNUSED, const Efl_Event *event EINA_UNUSED)
{
   efl_exit(0);
}

// Load a simple table layout into the window
static Efl_Ui_Table *
_ui_table_setup(Efl_Ui_Win *win)
{
   Efl_Ui_Table *table;
   int i;

   table = efl_add(EFL_UI_TABLE_CLASS, win);

   efl_pack_table_columns_set(table, 2);
   efl_ui_layout_orientation_set(table, EFL_UI_LAYOUT_ORIENTATION_HORIZONTAL);

   efl_add(EFL_UI_BUTTON_CLASS, win,
           efl_text_set(efl_added, "Long Button"),
           efl_pack_table(table, efl_added, 0, 2, 2, 1));

   for (i = 1; i <= 4; i++)
     {
        efl_add(EFL_UI_BUTTON_CLASS, win,
                efl_text_set(efl_added, eina_slstr_printf("Table %d", i)),
                efl_pack(table, efl_added));
     }

   return table;
}

// Load some boxes - a horizontal one for the window layout and a vertical
// one to contain a flow
static Efl_Ui_Box *
_ui_boxes_setup(Efl_Ui_Win *win)
{
   Efl_Ui_Box *box, *button;
   int i;

   box = efl_add(EFL_UI_BOX_CLASS, win,
                 efl_gfx_arrangement_content_padding_set(efl_added, 5, 0));

   for (i = 1; i <= 4; i++)
     {
        button = efl_add(EFL_UI_BUTTON_CLASS, win,
                         efl_text_set(efl_added, eina_slstr_printf("Boxed %d", i)),
                         efl_pack(box, efl_added));

        if (i == 2)
          efl_gfx_hint_size_max_set(button, EINA_SIZE2D(100, 50));
     }

    return box;
}

// Load a vertical and horizontal split into the window
static void
_ui_panes_setup(Efl_Ui_Win *win)
{
   Efl_Ui_Panes *split, *horiz_split;

   split = efl_add(EFL_UI_PANES_CLASS, win,
                   efl_content_set(win, efl_added),
                   efl_ui_panes_split_ratio_set(efl_added,  0.75));
   efl_content_set(efl_part(split, "first"), _ui_boxes_setup(win));

   horiz_split = efl_add(EFL_UI_PANES_CLASS, win,
                         efl_content_set(efl_part(split, "second"), efl_added),
                         efl_ui_layout_orientation_set(efl_added, EFL_UI_LAYOUT_ORIENTATION_HORIZONTAL),
                         efl_ui_panes_split_ratio_set(efl_added,  0.85));
   efl_content_set(efl_part(horiz_split, "first"), _ui_table_setup(win));

   efl_add(EFL_UI_BUTTON_CLASS, win,
           efl_text_set(efl_added, "Quit"),
           efl_gfx_hint_size_max_set(efl_added, EINA_SIZE2D(150, 30)),
           efl_content_set(efl_part(horiz_split, "second"), efl_added),
           efl_event_callback_add(efl_added, EFL_INPUT_EVENT_CLICKED,
                                  _gui_quit_cb, efl_added));
}

EAPI_MAIN void
efl_main(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   Eo *win;

   win = efl_add(EFL_UI_WIN_CLASS, efl_main_loop_get(),
                 efl_ui_win_type_set(efl_added, EFL_UI_WIN_TYPE_BASIC),
                 efl_text_set(efl_added, "Hello World"),
                 efl_ui_win_autodel_set(efl_added, EINA_TRUE));

   // when the user clicks "close" on a window there is a request to delete
   efl_event_callback_add(win, EFL_UI_WIN_EVENT_DELETE_REQUEST, _gui_quit_cb, NULL);

   _ui_panes_setup(win);

   efl_gfx_entity_size_set(win, EINA_SIZE2D(350, 250));
}
EFL_MAIN()


#define EFL_BETA_API_SUPPORT 1

#include <Elementary.h>
#include <Efl_Ui.h>

static void
_gui_about_clicked_cb(void *data EINA_UNUSED, const Efl_Event *event EINA_UNUSED)
{
   printf("Clicked About\n");
}

static void
_gui_quit_clicked_cb(void *data EINA_UNUSED, const Efl_Event *event EINA_UNUSED)
{
   printf("Clicked Quit\n");
   efl_exit(0);
}

static void
_focus_changed(void *data EINA_UNUSED, const Efl_Event *event)
{
   printf("Focus for object '%s' changed to %d\n",
          efl_text_get(event->object),  efl_ui_focus_object_focus_get(event->object));
}

static void
_gui_setup()
{
   Eo *win, *box, *hbox, *about;

   win = efl_add(EFL_UI_WIN_CLASS, efl_main_loop_get(),
                 efl_ui_win_type_set(efl_added, EFL_UI_WIN_TYPE_BASIC),
                 efl_text_set(efl_added, "Hello World"),
                 efl_event_callback_add(efl_added,
                                        EFL_UI_WIN_EVENT_DELETE_REQUEST,
                                        _gui_quit_clicked_cb, NULL),
                 efl_ui_win_autodel_set(efl_added, EINA_TRUE));

   box = efl_add(EFL_UI_BOX_CLASS, win,
                efl_content_set(win, efl_added),
                efl_gfx_hint_size_min_set(efl_added, EINA_SIZE2D(360, 240)));

  efl_add(EFL_UI_TEXTBOX_CLASS, box,
          efl_text_set(efl_added, "Label"),
          efl_text_interactive_editable_set(efl_added, EINA_FALSE),
          efl_event_callback_add(efl_added, EFL_UI_FOCUS_OBJECT_EVENT_FOCUS_CHANGED,
                                 _focus_changed, NULL),
          efl_pack(box, efl_added));

   hbox = efl_add(EFL_UI_BOX_CLASS, box,
                  efl_ui_layout_orientation_set(efl_added, EFL_UI_LAYOUT_ORIENTATION_HORIZONTAL),
                  efl_gfx_hint_weight_set(efl_added, 1.0, 0.1),
                  efl_pack(box, efl_added));

   about = efl_add(EFL_UI_BUTTON_CLASS, hbox,
                   efl_text_set(efl_added, "About"),
                   efl_pack(hbox, efl_added),
                   efl_event_callback_add(efl_added, EFL_UI_FOCUS_OBJECT_EVENT_FOCUS_CHANGED,
                                          _focus_changed, NULL),
                   efl_event_callback_add(efl_added, EFL_INPUT_EVENT_CLICKED,
                                          _gui_about_clicked_cb, efl_added));

   efl_add(EFL_UI_BUTTON_CLASS, hbox,
           efl_text_set(efl_added, "Quit"),
           efl_pack(hbox, efl_added),
           efl_event_callback_add(efl_added, EFL_UI_FOCUS_OBJECT_EVENT_FOCUS_CHANGED,
                                  _focus_changed, NULL),
           efl_event_callback_add(efl_added, EFL_INPUT_EVENT_CLICKED,
                                  _gui_quit_clicked_cb, efl_added));

   efl_ui_focus_util_focus(about);
}

EAPI_MAIN void
efl_main(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   _gui_setup();
}
EFL_MAIN()

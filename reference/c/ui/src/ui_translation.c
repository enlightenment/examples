#define EFL_BETA_API_SUPPORT 1

#include <stdio.h>

#include <Eina.h>
#include <Elementary.h>
#include <Efl_Ui.h>

/*
 * Efl.Ui translation exmaples.
 *
 * Efl.Ui contains a way to manage translations provided by gettext or
 * other translation provider. This example shows how to set it up and to
 * extract strings for translation. (see src/meson.build).
 *
 * The gettext extraction command is:
 * "xgettext --keyword=efl_ui_translatable_text_set:2 --from-code=utf-8 --foreign-user"
 *
 * In this project you can go to build/ and execute "ninja example_translation-pot"
 */
#define _TEXT_DOMAIN "example_translation"

// quit the app, called if the user clicks the Quit button or the window is deleted
static void
_gui_quit_cb(void *data EINA_UNUSED, const Efl_Event *event EINA_UNUSED)
{
   efl_exit(0);
}

EAPI_MAIN void
efl_main(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   Efl_Ui_Win *win, *box;

   win = efl_add(EFL_UI_WIN_CLASS, efl_main_loop_get(),
                 efl_ui_win_type_set(efl_added, EFL_UI_WIN_TYPE_BASIC),
                 efl_text_set(efl_added, "Translations"),
                 efl_ui_win_autodel_set(efl_added, EINA_TRUE));

   // when the user clicks "close" on a window there is a request to delete
   efl_event_callback_add(win, EFL_UI_WIN_EVENT_DELETE_REQUEST, _gui_quit_cb, NULL);

   box = efl_add(EFL_UI_BOX_CLASS, win,
                 efl_content_set(win, efl_added));

   efl_add(EFL_UI_BUTTON_CLASS, win,
           efl_ui_l10n_text_set(efl_added, "Translations", _TEXT_DOMAIN),
           efl_pack_end(box, efl_added));

   efl_add(EFL_UI_BUTTON_CLASS, win,
           efl_ui_l10n_text_set(efl_added, "Help", _TEXT_DOMAIN),
           efl_pack_end(box, efl_added));

   efl_add(EFL_UI_BUTTON_CLASS, win,
           efl_ui_l10n_text_set(efl_added, "Quit", _TEXT_DOMAIN),
           efl_pack_end(box, efl_added),
           efl_gfx_hint_size_min_set(efl_added, EINA_SIZE2D(100, 100)),
           efl_event_callback_add(efl_added, EFL_INPUT_EVENT_CLICKED,
                                  _gui_quit_cb, efl_added));

   efl_gfx_entity_size_set(win, EINA_SIZE2D(320, 320));
}
EFL_MAIN()


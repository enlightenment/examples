#define EFL_BETA_API_SUPPORT 1

#include <stdio.h>
#include <fcntl.h>

#include <Eina.h>
#include <Efl_Core.h>

/*
 * Efl.IO examples.
 *
 * This IO example shows the use of various IO APIs. First we use Efl.Io.File
 * to read and write a standard file. Then we set up a copier to copy data from
 * a source (stdin) to a destination (stdout) using an Efl.Io.Copier.
 */

static void
_io_write(const char *filename)
{
   Eina_Slice content = EINA_SLICE_STR("### This is a sample string for the file io test ###");
   Efl_Io_File *file;

   file = efl_new(EFL_IO_FILE_CLASS,
                  efl_file_set(efl_added, filename), // mandatory
                  efl_io_file_flags_set(efl_added, O_WRONLY | O_CREAT), // write and create - default is read
                  efl_io_file_mode_set(efl_added, 0644), // neccessary if we use O_CREAT
                  efl_io_closer_close_on_invalidate_set(efl_added, EINA_TRUE)); // recommended

   if (!file)
     return;

   printf("  Opened file %s for writing on fd %d\n", filename, efl_io_reader_fd_get(file));

   if (efl_io_writer_write(file, &content, NULL) != EINA_ERROR_NO_ERROR)
     fprintf(stderr, "  Failed to write test file\n");
   else
     {
        char *string = eina_slice_strdup(content);
        printf("  Wrote content: %s\n", string);
        free(string);
     }

   efl_unref(file);
}

static void
_io_read(const char *filename)
{
   EINA_RW_SLICE_DECLARE(content, 1024);
   Efl_Io_File *file;

   file = efl_new(EFL_IO_FILE_CLASS,
                  efl_file_set(efl_added, filename), // mandatory
                  efl_io_closer_close_on_invalidate_set(efl_added, EINA_TRUE)); // recommended

   if (!file)
     return;

   printf("  Opened file %s for reading on fd %d\n", filename, efl_io_reader_fd_get(file));

   if (efl_io_reader_read(file, &content) != EINA_ERROR_NO_ERROR)
     fprintf(stderr, "  Failed to read test file\n");
   else
     {
        char *string = eina_rw_slice_strdup(content);
        printf("  Read content: %s\n", string);
        free(string);
     }

   efl_unref(file);
}

static void
_io_test()
{
   const char *filename = "/tmp/efl_core_io_test.tmp";

   printf("TEST 1: Efl.Io.File\n"); 
   _io_write(filename);
   _io_read(filename);

   eina_file_unlink(filename);
}

static void
_copier_done(void *data EINA_UNUSED, const Efl_Event *event)
{
   fprintf(stderr, "  %s done\n", efl_name_get(event->object));

   efl_exit(EXIT_SUCCESS);
}

static void
_copier_error(void *data EINA_UNUSED, const Efl_Event *event)
{
   const Eina_Error *perr = event->info;

   fprintf(stderr, "  %s error: #%d '%s'\n",
           efl_name_get(event->object), *perr, eina_error_msg_get(*perr));

   efl_exit(EXIT_FAILURE);
}

EFL_CALLBACKS_ARRAY_DEFINE(copier_cbs,
                           { EFL_IO_COPIER_EVENT_DONE, _copier_done },
                           { EFL_IO_COPIER_EVENT_ERROR, _copier_error });

static void
_copier_test(Efl_Loop *loop)
{
   Eo *input, *output;
   Eo *copier = NULL;

   printf("TEST 2: Efl.Io.Copier\n");
   // set up our objects to copy, use stdin and stdout
   input = efl_add(EFL_IO_STDIN_CLASS, loop);
   output = efl_add(EFL_IO_STDOUT_CLASS, loop);

   // copier: set up a copy from input to output
   copier = efl_add(EFL_IO_COPIER_CLASS, loop,
                    efl_name_set(efl_added, "Copier"),
                    efl_io_copier_source_set(efl_added, input),
                    efl_io_copier_destination_set(efl_added, output),
                    efl_event_callback_array_add(efl_added, copier_cbs(), NULL));
   if (!copier)
     {
        fprintf(stderr, "  ERROR: could not create Efl_Io_Copier\n");
        efl_exit(EXIT_FAILURE);
     }

   printf("  Type something here and press enter, it will be copied to stdout...\n");
   printf("  (press Ctrl-D to exit)\n");
}

EAPI_MAIN void
efl_main(void *data EINA_UNUSED, const Efl_Event *ev)
{
   Efl_Loop *loop;

   _io_test();

   /*
    * Some objects such as the Efl.Io.Copier require a main loop,
    * thus their parent must be a loop provider.
    * We use the application's main loop itself.
    */
   loop = ev->object;
   _copier_test(loop);
}
EFL_MAIN()


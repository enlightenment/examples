#define EFL_BETA_API_SUPPORT 1

#include <stdio.h>

#include <Eina.h>
#include <Efl_Core.h>

/*
 * Efl Core Event examples.
 *
 * This example shows the various ways of adding callbacks for standard events.
 * It also demonstrates the creation of a custom event and how to freeze and
 * thaw events on an object.
 */


static void
_del_obj_cb(void *data EINA_UNUSED, const Efl_Event *event)
{
   printf("  Delete Object %s\n", efl_name_get(event->object));
}

static void
_custom_cb(void *data EINA_UNUSED, const Efl_Event *event)
{
   printf("  Custom event called on %s\n", efl_name_get(event->object));
}

static void
_poll_cb(void *data EINA_UNUSED, const Efl_Event *event)
{
   printf("  Poll from %s\n", efl_name_get(event->object));
}

static void
_freezethaw_cb(void *data, const Efl_Event *event)
{
   Efl_Loop *polled = data;
   static int called = 0;

   switch (called)
     {
        case 0:
          printf("  Freeze %s from %s\n", efl_name_get(polled), efl_name_get(event->object));
          efl_event_freeze(polled);
          break;
        case 1:
          printf("  Thaw %s from %s\n", efl_name_get(polled), efl_name_get(event->object));
          efl_event_thaw(polled);
          break;
        default:
          printf("  %s expired: exitting\n", efl_name_get(event->object));
          efl_exit(0);
     }

   called++;
}

static void
_events_freeze(Efl_Loop *mainloop)
{
   printf("Test 2:\n");

   // Set up a new poll on the main loop that we will interupt with freeze.
   efl_event_callback_add(mainloop, EFL_LOOP_EVENT_POLL_HIGH, _poll_cb, NULL);

   // notify every 0.1 seconds
   efl_add(EFL_LOOP_TIMER_CLASS, mainloop,
           efl_name_set(efl_added, "timer2"),
           efl_loop_timer_interval_set(efl_added, .1),
           efl_event_callback_add(efl_added, EFL_LOOP_TIMER_EVENT_TIMER_TICK, _freezethaw_cb, mainloop));
}

static void
_timer_cb(void *data EINA_UNUSED, const Efl_Event *event)
{
   Efl_Loop *mainloop = (Efl_Loop *)data;

   printf("  Timer reached for %s\n", efl_name_get(event->object));

   // stop listening to polling events
   efl_event_callback_del(mainloop, EFL_LOOP_EVENT_POLL_HIGH, _poll_cb, NULL);

   // cancel this timer
   efl_del(event->object);

   // setup second test
   _events_freeze(mainloop);
}

EFL_CALLBACKS_ARRAY_DEFINE(_callback_array,
{ EFL_LOOP_TIMER_EVENT_TIMER_TICK, _timer_cb },
{ EFL_EVENT_DEL, _del_obj_cb })

static void
_events_demo(Efl_Loop *mainloop)
{
   // this is a definition of a custom event, for our app
   Efl_Event_Description CUSTOM_EVENT = EFL_EVENT_DESCRIPTION("custom-event");

   printf("Test 1:\n");

   // add a single callback that gets called continuously
   efl_event_callback_add(mainloop, EFL_LOOP_EVENT_POLL_HIGH, _poll_cb, NULL);

   // here we add a custom callback and call it immediately
   efl_event_callback_add(mainloop, &CUSTOM_EVENT, _custom_cb, NULL);
   efl_event_callback_call(mainloop, &CUSTOM_EVENT, NULL);

   // we will exit from this timer after 0.1 seconds
   // add an array of callbacks (defined above)
   // this is more efficient if you have multiple callbacks to add
   efl_add(EFL_LOOP_TIMER_CLASS, mainloop,
           efl_name_set(efl_added, "timer1"),
           efl_loop_timer_interval_set(efl_added, 0.1),
           efl_event_callback_array_add(efl_added, _callback_array(), mainloop));

   printf("  Waiting for timer1 to call back...\n");
}

EAPI_MAIN void
efl_main(void *data EINA_UNUSED, const Efl_Event *ev)
{
   Efl_Loop *mainloop;

   mainloop = ev->object;
   efl_name_set(mainloop, "mainloop");
   _events_demo(mainloop);
}
EFL_MAIN()


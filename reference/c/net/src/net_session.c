#define EFL_BETA_API_SUPPORT 1

#include <stdio.h>

#include <Eina.h>
#include <Efl_Net.h>

/*
 * Efl.Net session/connectivity examples.
 *
 * NOTE: This example currently requires the Connman backend to be running.
 *
 * This example uses the Efl.Net.Session APIs to get connectivity information
 * about the current networking setup. It then sets up a callback for network
 * changes and will print the details on any change.
 */

// Convert a session state to a string for printing.
static const char *
_state_name(Efl_Net_Session_State state)
{
   switch (state)
     {
      case EFL_NET_SESSION_STATE_OFFLINE:
        return "offline";
      case EFL_NET_SESSION_STATE_LOCAL:
        return "local";
      case EFL_NET_SESSION_STATE_ONLINE:
        return "online";
      default:
        return "???";
   }
}

// Convert a session technology to a string for printing.
static const char *
_technology_name(Efl_Net_Session_Technology tech)
{
   static const char *names[] = {
     [EFL_NET_SESSION_TECHNOLOGY_UNKNOWN] = "unknown",
     [EFL_NET_SESSION_TECHNOLOGY_ETHERNET] = "ethernet",
     [EFL_NET_SESSION_TECHNOLOGY_WIFI] = "wifi",
     [EFL_NET_SESSION_TECHNOLOGY_BLUETOOTH] = "bluetooth",
     [EFL_NET_SESSION_TECHNOLOGY_CELLULAR] = "cellular",
     [EFL_NET_SESSION_TECHNOLOGY_VPN] = "vpn",
     [EFL_NET_SESSION_TECHNOLOGY_GADGET] = "gadget",
   };

   if ((unsigned)tech > sizeof(names)/sizeof(names[0])
       || !names[tech])
     return "???";

   return names[tech];
}

// Tthe callback that indicates some connectivity changed within the session.
// Print information about the session to the console.
static void
_session_changed(void *data EINA_UNUSED, const Efl_Event *event)
{
   Eo *session = event->object;
   const char *ip, *netmask, *gateway;
   uint8_t prefix;

   printf("Session changed:\n"
          "  name: '%s'\n"
          "  state: %s\n"
          "  technology: %s\n"
          "  interface: '%s'\n",
          efl_net_session_network_name_get(session),
          _state_name(efl_net_session_state_get(session)),
          _technology_name(efl_net_session_technology_get(session)),
          efl_net_session_interface_get(session));

   // print out additional information if we have an IPv4 session
   efl_net_session_ipv4_get(session, &ip, &netmask, &gateway);
   if (ip)
     {
        printf("  IPv4: %s, gateway=%s, netmask=%s\n",
               ip, gateway, netmask);
     }

   // print out additional information if we have an IPv6 session
   efl_net_session_ipv6_get(session, &ip, &prefix, &netmask, &gateway);
   if (ip)
     {
        printf("  IPv6: %s/%hhu, gateway=%s, netmask=%s\n",
               ip, prefix, gateway, netmask);
     }
}

// Quit the app after a timer tick.
static void
_quit_cb(void *data EINA_UNUSED, const Efl_Event *event EINA_UNUSED)
{
   efl_exit(0);
}

EAPI_MAIN void
efl_main(void *data EINA_UNUSED, const Efl_Event *ev)
{
   Eina_Bool do_connect = EINA_TRUE;
   Eina_Bool require_online = EINA_FALSE;
   Efl_Net_Session_Technology technologies;
   Efl_Net_Session *session;
   Efl_Loop *loop;

   loop = ev->object;
   // create a session that watches specifically for ethernet, wifi and bluetooth
   technologies = EFL_NET_SESSION_TECHNOLOGY_ETHERNET |
       EFL_NET_SESSION_TECHNOLOGY_WIFI | EFL_NET_SESSION_TECHNOLOGY_BLUETOOTH;
   session = efl_add(EFL_NET_SESSION_CLASS, loop,
                     efl_name_set(efl_added, "Example Session"),
                     // register the change callback for network state
                     efl_event_callback_add(efl_added, EFL_NET_SESSION_EVENT_CHANGED,
                                            _session_changed, NULL));
   if (!session)
     {
        EINA_LOG_ERR("Could not create Efl.Net.Session object.\n");
        efl_exit(EXIT_FAILURE);
        return;
     }

   if (do_connect)
     {
        printf("Requesting a %s connection.\n", require_online ? "online" : "local");
        efl_net_session_connect(session, require_online, technologies);
     }

   printf("The session will remain active while this application runs.\n");
   printf("Use ^C (Control + C) to close it\n");

   // Wait for 10 seconds before exiting this example
   efl_add(EFL_LOOP_TIMER_CLASS, loop,
           efl_loop_timer_interval_set(efl_added, 10.0),
           efl_event_callback_add(efl_added, EFL_LOOP_TIMER_EVENT_TIMER_TICK,
                                  _quit_cb, NULL));
}
EFL_MAIN()

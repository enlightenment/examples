/*
 * Efl.UI custom widget examples.
 *
 * Inherit from an EFL C# class and customize it
 */

using System;

// This is our own button with customized text functions
public class MyButton : Efl.Ui.Button
{
    // This id shows how our data is preserved when overriden methods
    // are called from native code
    private int button_id;

    // Constructor sets an initial text
    public MyButton(Efl.Object parent, int id = 0) :
        base(parent)
    {
        button_id = id;
        base.Text = "Base text for button id " + id;
    }

    // This calls the parent's SetText() method with a modified string
    public override string Text
    {
        set {
           base.Text = "Overriden text for button id " + button_id + ": " + value;
        }
    }
}

public class Example : Efl.Csharp.Application
{
    protected override void OnInitialize(string[] args)
    {
        // Create a window and initialize it
        Efl.Ui.Win win = new Efl.Ui.Win(null, winType: Efl.Ui.WinType.Basic);
        win.Text = "Custom widget demo";
        win.Autohide = true;
        win.VisibilityChangedEvent += (object sender, Efl.Gfx.EntityVisibilityChangedEventArgs e) => {
          // Exit the EFL main loop when the window is closed
          if (e.Arg == false)
              Efl.Ui.Config.Exit();
        };
        // Give the window an initial size
        win.Size = new Eina.Size2D(350,250);

        // Instantiate our custom button widget
        MyButton btn = new MyButton(win, 99);
        btn.ClickedEvent += (object sender, Efl.Input.ClickableClickedEventArgs e) => {
          // When the button is clicked, change its text
          MyButton b = (MyButton)sender;
          b.Text = "Hello!";
        };

        win.SetContent(btn);
    }
    public static void Main()
    {
        var example = new Example();
        example.Launch();
    }
}

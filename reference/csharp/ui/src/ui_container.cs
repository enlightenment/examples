/*
 * Efl.UI container examples.
 *
 * Load and pack a selection of containers.
 * Each has its own unique layout and methods which are demonstrated below.
 */

using System;

public class Example : Efl.Csharp.Application
{
    // Create a box container full of buttons
    static Efl.Ui.Box CreateBox(Efl.Ui.Win win)
    {
        Efl.Ui.Box box = new Efl.Ui.Box(win);
        // Set distance between contained elements
        box.ContentPadding = (5, 0);

        for (int i = 1; i <= 4; ++i)
        {
            // Add 4 buttons, one below the other
            var button = new Efl.Ui.Button(win);
            button.Text = $"Boxed {i}";
            if (i == 2)
            {
              // Button 2 has its maximum size limited, so it will be smaller
              button.HintSizeMax = new Eina.Size2D(100,50);
            }
            box.Pack(button);
        }

        return box;
    }

    // Create a simple table layout
    static Efl.Ui.Table CreateTable(Efl.Ui.Win win)
    {
        Efl.Ui.Table table = new Efl.Ui.Table(win);
        // Table with two columns, that get filled left to right, and then top to bottom
        table.TableColumns = 2;
        table.Orientation = Efl.Ui.LayoutOrientation.Horizontal;
        Efl.Ui.Button button;

        for (int i = 1; i <= 4; ++i)
        {
            // Add 4 buttons, following the defined table flow
            button = new Efl.Ui.Button(win);
            button.Text = $"Table {i}";
            table.Pack(button);
        }

        // Last button spans two table cells
        button = new Efl.Ui.Button(win);
        button.Text = "Long Button";
        table.PackTable(button, 0, 2, 2, 1);

        return table;
    }

    protected override void OnInitialize(string[] args)
    {
        // Create a window and initialize it
        Efl.Ui.Win win = new Efl.Ui.Win(null, winType: Efl.Ui.WinType.Basic);
        win.Text = "Container demo";
        win.Autohide = true;
        win.VisibilityChangedEvent += (object sender, Efl.Gfx.EntityVisibilityChangedEventArgs e) => {
          // Exit the EFL main loop
          if (e.Arg == false)
              Efl.Ui.Config.Exit();
        };
        // Give the window an initial size so there is room to resize the panes.
        // Otherwise, all widgets are tightly packed
        win.Size = new Eina.Size2D(350,250);

        // Create a vertically-split panes container
        Efl.Ui.Panes vsplit = new Efl.Ui.Panes(win);
        vsplit.SplitRatio = 0.75;
        win.Content = vsplit;

        // Create some boxes and set them as the content of the first pane of the container
        var box = CreateBox(win);
        vsplit.FirstPart.Content = box;

        // Create a second, horizontally-split panes container and set it as the content of
        // the second pane of the first container
        Efl.Ui.Panes hsplit = new Efl.Ui.Panes(win);
        hsplit.Orientation = Efl.Ui.LayoutOrientation.Horizontal;
        hsplit.SplitRatio = 0.85;
        vsplit.SecondPart.SetContent(hsplit);

        // Create a table and set it as the content of the first pane of the horizontal
        // container
        var table = CreateTable(win);
        hsplit.FirstPart.SetContent(table);

        // Create a button and set it as the content of the second pane of the horizontal
        // container
        Efl.Ui.Button quit_btn = new Efl.Ui.Button(win);
        quit_btn.Text = "Quit";
        quit_btn.HintSizeMax = new Eina.Size2D(150, 30);
        quit_btn.ClickedEvent += (object sender, Efl.Input.ClickableClickedEventArgs e) => {
            // Exit the EFL main loop
            Efl.Ui.Config.Exit();
        };
        hsplit.SecondPart.SetContent(quit_btn);
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch();
    }
}


using System;

public class Example : Efl.Csharp.Application
{
    public static void FocusChangedCb(object sender, EventArgs e)
    {
        Console.WriteLine($"Focus for object {((Efl.IText)sender).Text} changed to {((Efl.Ui.Widget)sender).Focus}");
    }

    protected override void OnInitialize(string[] args)
    {
        // Create a window and initialize it
        var win = new Efl.Ui.Win(null, winType: Efl.Ui.WinType.Basic);
        win.Text = "Focus example";
        win.Autohide = true;
        win.VisibilityChangedEvent += (object sender, Efl.Gfx.EntityVisibilityChangedEventArgs e) => {
            // Exit the EFL main loop
            if (e.Arg == false)
                Efl.Ui.Config.Exit();
        };


        // Create the main box container
        var vbox = new Efl.Ui.Box(win);
        vbox.HintSizeMin = new Eina.Size2D(360, 240);
        win.Content = vbox;

        // Create some check boxes
        Efl.Ui.Check first_checkbox = null;
        for (int i = 0; i< 5; i++) {
          var checkbox = new Efl.Ui.Check(vbox);
          checkbox.Text = "Check " + i;
          checkbox.HintFill = (false, false);
          checkbox.HintAlign = ((Efl.Gfx.Align)0.5, (Efl.Gfx.Align)0.5);
          checkbox.FocusChangedEvent += FocusChangedCb;
          vbox.Pack(checkbox);
          if (i == 0) first_checkbox = checkbox;
        };

        // Create an horizontal box to contain the two buttons
        var hbox = new Efl.Ui.Box(vbox);
        hbox.Orientation = Efl.Ui.LayoutOrientation.Horizontal;
        vbox.Pack(hbox);

        // Create a "Focus Mover" button
        var button = new Efl.Ui.Button(hbox);
        button.Text = "Focus mover";
        button.FocusChangedEvent += FocusChangedCb;
        button.ClickedEvent += (object sender, Efl.Input.ClickableClickedEventArgs e) => {
            Console.WriteLine("Clicked Focus Mover");
            // Manually transfer focus to the first check box
            Efl.Ui.Focus.Util.Focus(first_checkbox);
        };
        hbox.Pack(button);

        // Create a Quit button
        button = new Efl.Ui.Button(hbox);
        button.Text = "Quit";
        button.FocusChangedEvent += FocusChangedCb;
        button.ClickedEvent += (object sender, Efl.Input.ClickableClickedEventArgs e) => {
            Console.WriteLine("Clicked Quit");
            Efl.Ui.Config.Exit();
        };
        hbox.Pack(button);

        // Show the focus highlight
        win.FocusHighlightEnabled = true;
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch();
    }
}


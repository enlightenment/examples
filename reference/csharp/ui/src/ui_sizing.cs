/*
 * Efl.UI sizing examples.
 *
 * Demonstrate how to use the sizing api from Efl.Gfx.
 * We load a box with 3 buttons, one with default sizing, one that has a max size
 * and the last one has a min size. Try resizing the window to see how this changes.
 */

using System;

public class Example : Efl.Csharp.Application
{
    protected override void OnInitialize(string[] args)
    {
        // Create a window and initialize it
        Efl.Ui.Win win = new Efl.Ui.Win(null, winType: Efl.Ui.WinType.Basic);
        win.Text = "Size Control";
        win.Autohide = true;
        win.VisibilityChangedEvent += (object sender, Efl.Gfx.EntityVisibilityChangedEventArgs e) => {
            // Exit the EFL main loop
            if (e.Arg == false)
                Efl.Ui.Config.Exit();
        };

        // Create a box container
        Efl.Ui.Box box = new Efl.Ui.Box(win);
        win.SetContent(box);

        // Create a regular button (without size hints)
        var button = new Efl.Ui.Button(win);
        button.Text = "Button";
        box.Pack(button);

        // Create a small button (max size is limited)
        button = new Efl.Ui.Button(win);
        button.Text = "Small";
        button.HintSizeMax = new Eina.Size2D(50,50);
        box.Pack(button);

        // Create a big button (min size is limited)
        button = new Efl.Ui.Button(win);
        button.Text = "Big button";
        button.HintSizeMin = new Eina.Size2D(100,100);
        box.Pack(button);
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch();
    }
}


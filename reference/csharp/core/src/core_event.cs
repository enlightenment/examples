/*
 * Efl Core Event examples.
 *
 * This example shows the various ways of adding callbacks for standard events.
 * It also demonstrates how to freeze and thaw events on an object.
 */

using System;

public class Example : Efl.Csharp.Application
{
    // Polling callback
    private static void PollCb(object sender, EventArgs e)
    {
        Console.WriteLine("  Poll from {0}", ((Efl.Object)sender).Name);
    }

    protected override void OnInitialize(string[] args)
    {
        // Retrieve the application's main loop
        var mainloop = Efl.App.AppMain;
        mainloop.Name = "Mainloop";

        // This event gets triggered continuously
        mainloop.PollHighEvent += PollCb;

        // This timer will control events fired by the main loop
        var timer = new Efl.LoopTimer(mainloop, 0.1);
        timer.Name = "Timer";
        // To count number of timer triggers
        int tick_count = 0;
        timer.TimerTickEvent += (object sender, EventArgs e) => {
            string message = "Tick {0} from {1}: ";
            // Depending on the number of timer ticks, it does a different thing
            switch (tick_count) {
                case 0:
                    message += "Freezing Mainloop events";
                    mainloop.FreezeEvent();
                    break;
                case 1:
                    message += "Thawing Mainloop events";
                    mainloop.ThawEvent();
                    break;
                default:
                    message += "Quitting";
                    mainloop.Quit(new Eina.Value(0));
                    break;
            }
            Console.WriteLine(message, tick_count, ((Efl.Object)sender).Name);
            tick_count++;
        };

        Console.WriteLine("Waiting for Timer to call back...");
    }

    protected override void OnTerminate()
    {
        Console.WriteLine("Application is over");
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch(Efl.Csharp.Components.Basic);
    }
}


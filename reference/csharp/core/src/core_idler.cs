/*
 * Efl Core Idler examples.
 *
 * Here we register callbacks to execute code when the loop is idle.
 * We also record when we enter or exit the idle state.
 *
 * We initiate a timer to exit the idle state and then exit the application.
 */

using System;

public class Example : Efl.Csharp.Application
{
    protected override void OnInitialize(string[] args)
    {
        // Retrieve the application's main loop
        var mainloop = Efl.App.AppMain;

        // Register to all Idle events
        mainloop.IdleEnterEvent += (object sender, EventArgs e) => {
          Console.WriteLine("IDLE ENTER: Entering idle state.");
        };
        mainloop.IdleEvent += (object sender, EventArgs e) => {
          Console.WriteLine("IDLE: Executing idler callback while in idle state.");
        };
        mainloop.IdleExitEvent += (object sender, EventArgs e) => {
          Console.WriteLine("IDLE EXIT: Leaving idle state.");
        };

        // Use a timer to exit the application
        var timer = new Efl.LoopTimer(mainloop, 0.02);
        timer.TimerTickEvent += (object sender, EventArgs e) => {
          Console.WriteLine("TIMER: timer callback called, exiting.");
          mainloop.Quit(0);
        };
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch(Efl.Csharp.Components.Basic);
    }
}

/*
 * Efl Core Poll examples.
 *
 * This example sets up poll callbacks for LOW, MEDIUM and HIGH frequency events.
 * We run for 30 seconds and print to stdout to show when each is called.
 * Depending on your system this may not include any LOW frequency polls.
 */

using System;

public class Example : Efl.Csharp.Application
{
    protected override void OnInitialize(string[] args)
    {
        // Retrieve the application's main loop
        var mainloop = Efl.App.AppMain;

        // Register to all Poll events
        mainloop.PollLowEvent += (object sender, EventArgs e) => {
          Console.Write("L");
        };
        mainloop.PollMediumEvent += (object sender, EventArgs e) => {
          Console.Write("M");
        };
        mainloop.PollHighEvent += (object sender, EventArgs e) => {
          Console.Write(".");
        };

        // Use a timer to exit the application
        var timer = new Efl.LoopTimer(mainloop, 30);
        timer.TimerTickEvent += (object sender, EventArgs e) => {
          Console.WriteLine("\nTIMER: timer callback called, exiting.");
          mainloop.Quit(0);
        };
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch(Efl.Csharp.Components.Basic);
    }
}

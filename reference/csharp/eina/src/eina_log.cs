
/*
 * Eina.Log examples.
 *
 * This demo shows how to log at various levels and to change what log is shown.
 * You can also use a custom log printer in your app as shown in _log_custom.
 */

using System;

public class Example : Efl.Csharp.Application
{
    static double Divide(int num, int denom)
    {
        if (denom == 0)
            Eina.Log.Critical("Attempt to divide by 0\n");
        else
        {
            if (denom < 0)
                Eina.Log.Warning("Possible undesirable effect, divide by negative number");

            double ret = ((double) num / denom);
            Eina.Log.Info($"{num} / {denom} = {ret}\n");
            return ret;
        }

        return -1;
    }

    static void Divides()
    {
        Divide(5, 1);
        Divide(5, -1);
        Divide(5, 0);
    }

    static void LogLevels()
    {
        Console.WriteLine("Executing with default logging");
        Divides();

        Eina.Log.GlobalLevelSet(Eina.Log.Level.Warning);
        Console.WriteLine("Executing with Warning level"); // Same as EINA_LOG_LEVEL = 2
        Divides();

        Eina.Log.GlobalLevelSet(Eina.Log.Level.Info);
        Console.WriteLine("Executing with Info on"); // Same as EINA_LOG_LEVEL = 3
        Divides();
    }

    protected override void OnInitialize(string[] args)
    {
        LogLevels();

        // TODO: missing
        //LogCustom();

        Efl.App.AppMain.Quit(0);
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch(Efl.Csharp.Components.Basic);
    }
}

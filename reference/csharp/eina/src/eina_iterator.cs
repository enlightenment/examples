
/*
 * Eina.Iterator examples.
 *
 * These examples demonstrate how to work with Eina.Iterator methods.
 * Both an Eina.List and an Eina.Array are created and an iterator obtained
 * for both. You can see how we can use iterators irrespective of the source
 * and also that there are different ways to work with iterating content.
 */

using System;

public class Example : Efl.Csharp.Application
{
    static void PrintIterator(Eina.Iterator<string> it)
    {
        Console.WriteLine("--iterator start--");
        foreach(string s in it)
            Console.WriteLine(s);
        Console.WriteLine("-- iterator end --");
    }

    static Eina.Array<string> CreateArray()
    {
        string[] strings =
        {
            "name strings",
            "husker",
            "starbuck",
            "boomer"
        };

        var array = new Eina.Array<string>(4u);

        foreach (string s in strings)
            array.Push(s);

        return array;
    }

    static Eina.List<string> CreateList()
    {
        string[] more_strings = {
            "sentence strings",
            "what do your hear?",
            "nothing but the rain",
            "then grab your gun and bring the cat in"
        };

        var list = new Eina.List<string>();

        foreach (string s in more_strings)
            list.Append(s);

        return list;
    }

    protected override void OnInitialize(string[] args)
    {
        // Create an Eina.Array and iterate through its contents
        var array = CreateArray();
        var it = array.GetIterator();
        PrintIterator(it);
        it.Dispose();
        array.Dispose();

        // Perform the same iteration with an Eina.List
        var list = CreateList();
        it = list.GetIterator();
        PrintIterator(it);
        it.Dispose();
        list.Dispose();

        Efl.App.AppMain.Quit(0);
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch(Efl.Csharp.Components.Basic);
    }
}

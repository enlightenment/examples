/*
 * Eina.Value examples.
 *
 * These examples demonstrate how to work with Eina.Value data and methods.
 * Eina.Value is a way to represent and pass data of varying types and to
 * convert efficiently between them.
 * Eina.Value can even define structs for managing more complex requirements.
 */

using System;

public class Example : Efl.Csharp.Application
{
    static void ValueInt()
    {
        // Setting up an integer value type
        Eina.Value int_val = 123;
        Console.WriteLine("int_val value is {0}", int_val);

        // It can easily be converted to a string
        string str = int_val.ToString();
        Console.WriteLine("int_val to string is \"{0}\"", str);
    }

    static void ValueString()
    {
        // Setting up an string value type
        Eina.Value str_val = "My string";
        Console.WriteLine("str_val value is \"{0}\"", str_val);

        // To string should have the same content
        string newstr = str_val.ToString();
        Console.WriteLine("str_val to string is \"{0}\"", newstr);
    }

    static void ValueConvert()
    {
        // Convert from int to string:
        Eina.Value int_val = 123;
        Eina.Value str_val = new Eina.Value(Eina.ValueType.String);
        int_val.ConvertTo(str_val);
        Console.WriteLine("int_val was {0}, converted to string is \"{1}\"", int_val, str_val);

        // And the other way around!
        str_val = "33.000";
        str_val.ConvertTo(int_val);
        Console.WriteLine("str_val was \"{0}\", converted to int is {1}", str_val, int_val);
    }

    protected override void OnInitialize(string[] args)
    {
        ValueInt();
        Console.WriteLine("");

        ValueString();
        Console.WriteLine("");

        ValueConvert();
        Console.WriteLine("");

        Efl.App.AppMain.Quit(0);
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch(Efl.Csharp.Components.Basic);
    }
}

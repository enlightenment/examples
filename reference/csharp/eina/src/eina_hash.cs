/*
 * Eina.Hash examples.
 *
 * These examples demonstrate how to work with Eina.Hash data and methods.
 *
 * We have two main hash objects here, firstly an int keyed hash with some
 * dummy content.
 * The second example is that of a simple telephone book. The names are the
 * string keys to the hash with phone numbers in the data field.
 */

using System;
using System.Collections.Generic;

public class Example : Efl.Csharp.Application
{
    static Eina.Hash<Int32, string> CreateHash()
    {
        // Let's create a simple hash with integers as keys
        var hash = new Eina.Hash<Int32, string>();

        // Add initial entries to our hash
        for (int i = 0; i < 10; ++i)
            hash.Add(i, $"The number {i}");

        return hash;
    }

    static void HashDemo()
    {
        var hash = CreateHash();

        // Get an iterator of the keys so we can print a line per entry
        var iter = hash.Keys();

        Console.WriteLine("Print contents of int hash");
        foreach (int key in iter)
        {
            // Look up the value for the key so we can print both
            string value = hash.Find(key);
            Console.WriteLine($"  Item found with id {key} has value {value}");
        }
        iter.Dispose();
        Console.WriteLine("");

        hash.Dispose();
    }

    // Here we begin the phone book example

    static void PrintPhonebookEntry(string key, string data)
    {
        Console.WriteLine($"  Name: {key}\tNumber {data}\n");
    }

    static void PrintPhonebook(Eina.Hash<string, string> book)
    {
        int count = book.Population();
        Console.WriteLine($"Complete phone book ({count}):");

        // As an enumerator, iterate over the key and value for each entry
        foreach (KeyValuePair<string, string> kvp in book)
            PrintPhonebookEntry(kvp.Key, kvp.Value);

        Console.WriteLine("");
    }

    static Eina.Hash<string, string> CreatePhonebook()
    {
        string[] names =
        {
            "Wolfgang Amadeus Mozart", "Ludwig van Beethoven",
            "Richard Georg Strauss", "Heitor Villa-Lobos"
        };
        string[] numbers =
        {
            "+01 23 456-78910", "+12 34 567-89101",
            "+23 45 678-91012", "+34 56 789-10123"
        };

        // Create hash of strings to strings
        var hash = new Eina.Hash<string, string>();

        // Add initial entries to our hash
        for (int i = 0; i < 4; ++i)
            hash.Add(names[i], numbers[i]);

        return hash;
    }

    static void PhonebookDemo()
    {
        string lookup_name = "Ludwig van Beethoven";

        var phone_book = CreatePhonebook();
        PrintPhonebook(phone_book);

        string number = phone_book.Find(lookup_name);
        Console.WriteLine("Found entry:");
        PrintPhonebookEntry(lookup_name, number);
        Console.WriteLine("");

        // Let's first add a new entry
        phone_book["Raul Seixas"] = "+55 01 234-56789";

        // Change the phone number for an entry
        string old_num = phone_book[lookup_name];
        phone_book[lookup_name] = "+12 34 222-22222";
        Console.WriteLine($"Old number for {lookup_name} was {old_num}\n");

        // Change the name (key) on an entry
        phone_book.Move("Raul Seixas", "Alceu Valenca");

        Console.WriteLine("After modifications");
        PrintPhonebook(phone_book);

        phone_book.Dispose();
    }

    protected override void OnInitialize(string[] args)
    {
        HashDemo();
        PhonebookDemo();

        Efl.App.AppMain.Quit(0);
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch(Efl.Csharp.Components.Basic);
    }
}

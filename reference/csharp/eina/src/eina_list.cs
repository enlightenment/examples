
/*
 * Eina.List examples.
 *
 * These examples demonstrate how to work with Eina.List data and methods.
 * We create a simple list of names by appending strings to an empty list
 * and then run various mutations and print each result.
 */

using System;
using System.Linq;

public class Example : Efl.Csharp.Application
{
    static Eina.List<string> CreateList()
    {
        var list = new Eina.List<string>();

        list.Append("Adama");
        list.Append("Baltar");
        list.Append("Roslin");

        return list;
    }

    protected override void OnInitialize(string[] args)
    {
        var list = CreateList();

        // Print our list with a simple foreach
        Console.WriteLine("List size: {0}", list.Count());
        Console.WriteLine("List content:");
        foreach(string item in list)
            Console.WriteLine("  {0}", item);

        // Insert some more elements
        list.Prepend("Cain");
//         list.PrependRelative("Tigh", "Baltar"); // TODO: missing

        Console.WriteLine("New list content:");
        foreach(string item in list)
            Console.WriteLine("  {0}", item);

        // Promote an item to the top of the list
        // TODO: implement ?
//         list.PromoteList(list.NthList(1));
//         list.Remove("Cain");
//
//         Console.WriteLine("List content after promotion:");
//         foreach(string item in list)
//             Console.WriteLine("  {0}", item);

        // We can sort the list with any callback
//         list.Sort((string strA, string strB) => { return strA.Compare(strB); }); // TODO: FIXME custom sort
        list.Sort();
        Console.WriteLine("List content sorted:");
        foreach(string item in list)
            Console.WriteLine("  {0}", item);

        // And foreach can be in reverse too
        Console.WriteLine("List content reverse sorted:");
        foreach(string item in list.Reverse())
            Console.WriteLine("  {0}", item);

        list.Dispose();

        Efl.App.AppMain.Quit(0);
    }

    public static void Main()
    {
        var example = new Example();
        example.Launch(Efl.Csharp.Components.Basic);
    }
}

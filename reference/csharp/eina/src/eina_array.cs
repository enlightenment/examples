/*
 * Eina.Array examples.
 *
 * These examples demonstrate how to work with Eina.Array data and methods.
 * We use a simple array of strings to initialise our Eina.Array before
 * performing various mutations and printing the results.
 */

using System;

public class Example : Efl.Csharp.Application
{
    static Eina.Array<string> CreateArray()
    {
        // Some content to populate our array
        string[] names =
        {
            "helo", "hera", "starbuck", "kat", "boomer",
            "hotdog", "longshot", "jammer", "crashdown", "hardball",
            "duck", "racetrack", "apolo", "husker", "freaker",
            "skulls", "bulldog", "flat top", "hammerhead", "gonzo"
        };

        // Set up an array with a growth step to give a little headroom
        var array = new Eina.Array<string>(25u);

        foreach (string name in names)
            array.Push(name);

        return array;
    }

    static bool ItemRemoveCb(string name)
    {
        // Let's keep any strings that are no more than 7 characters long
        if (name.Length <= 7)
            return false;
        return true;
    }

    protected override void OnInitialize(string[] args)
    {
        var array = CreateArray();

        // Show the contents of our array
        Console.WriteLine("Array count: {0}", array.Count);
        Console.WriteLine("Array contents:");
        foreach(string name in array)
        {
            // Content is strings so we simply print the data
            Console.WriteLine("  {0}", name);
        }

        // Access a specific item in the array
        Console.WriteLine("Top gun: {0}", array[2]);

        // Update a single item in the array
        array[17] = "flattop";
        // Update the array removing items that match the ItemRemoveCb criteria
        // array.RemoveAll(ItemRemoveCb); // TODO: FIXME

        // Print the new contents of our array
        Console.WriteLine("New array count: {0}", array.Count);
        Console.WriteLine("New array contents:");
        foreach(string name in array)
            Console.WriteLine("  {0}", name);

        array.Dispose();

        Efl.App.AppMain.Quit(0);
    }

#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var example = new Example();
        example.Launch(Efl.Csharp.Components.Basic);
    }
}


Efl.Ui.Check check = new Efl.Ui.Check(parent);

check.Text = "Test Check";
check.SetSelected(true);

check.SelectedChangedEvent += (sender, args) =>
{
    if (check.Selected)
        Console.WriteLine("Check is selected");
    else
        Console.WriteLine("Check is not selected");
};

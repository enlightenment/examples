Efl.Ui.Table table = new Efl.Ui.Table(parent);

table.SetTableSize(2, 2);

Efl.Ui.Button button1 = new Efl.Ui.Button(table);
Efl.Ui.Button button2 = new Efl.Ui.Button(table);

// The first column and row have indexes = 0.
table.PackTable(button1, 0, 0, 1, 1);
table.PackTable(button2, 1, 1, 1, 1);

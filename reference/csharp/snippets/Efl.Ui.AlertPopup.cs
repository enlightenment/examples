Efl.Ui.AlertPopup alertPopup = new Efl.Ui.AlertPopup(parent);

alertPopup.SetButton(Efl.Ui.AlertPopupButton.Positive, "Accept", null);
alertPopup.SetButton(Efl.Ui.AlertPopupButton.Negative, "Reject", null);

alertPopup.ButtonClickedEvent += (sender, args) =>
{
    if (args.arg.Button_type.Equals(Efl.Ui.AlertPopupButton.Positive))
        Console.WriteLine("Positive action invoked");
    else if (args.arg.Button_type.Equals(Efl.Ui.AlertPopupButton.Negative))
        Console.WriteLine("Negative action invoked");
};

alertPopup.BackwallClickedEvent += (s, e) =>
{
    Console.WriteLine("Backwall clicked");
};

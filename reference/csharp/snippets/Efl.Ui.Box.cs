Efl.Ui.Box box = new Efl.Ui.Box(parent);

//Creating content which we will pack into the box
//It can be any widget, for example, buttons
Efl.Ui.Button button1 = new Efl.Ui.Button(box);
Efl.Ui.Button button2 = new Efl.Ui.Button(box);

//Packing the content to the box, one after another
box.Pack(button1);
box.Pack(button2);

Efl.Ui.Button button = new Efl.Ui.Button(parent);

button.Text = "Test Button";

button.ClickedEvent += (sender, args) =>
{
    Efl.Ui.Button btn = (Efl.Ui.Button)sender;
    btn.Text = "Clicked";
};

Efl.Ui.Scroller scroller = new Efl.Ui.Scroller(parent);

// Create a large image to put it inside the scroller
Efl.Ui.Image image = new Efl.Ui.Image(scroller);

image.HintSizeMin = new Eina.Size2D(1000, 1000);
image.SetFile(image_path + "image.png");
image.Load();

scroller.SetContent(image);

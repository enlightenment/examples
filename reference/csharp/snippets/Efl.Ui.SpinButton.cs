Efl.Ui.SpinButton spinButton = new Efl.Ui.SpinButton(parent);

spinButton.Orientation = Efl.Ui.LayoutOrientation.Vertical;

spinButton.SetRangeLimits(0, 100);
spinButton.SetRangeStep(2);
spinButton.SetRangeValue(50);

spinButton.ChangedEvent += (sender, args) =>
{
    Efl.Ui.SpinButton spnBtn = (Efl.Ui.SpinButton)sender;
    Console.WriteLine("Range value changed to: " + spnBtn.RangeValue);
};

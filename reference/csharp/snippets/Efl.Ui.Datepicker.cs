Efl.Ui.Datepicker datepicker = new Efl.Ui.Datepicker(parent);

datepicker.SetDateMin(2000, 1, 1);
datepicker.SetDateMax(2030, 1, 1);
datepicker.SetDate(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

datepicker.DateChangedEvent += (sender, args) =>
{
    datepicker.GetDate(out int year, out int month, out int day);
    Console.WriteLine("Date has been changed! Current date: " + year + "-" + month + "-" + day);
};

Efl.Ui.Popup popup = new Efl.Ui.Popup(parent);

Efl.Ui.Button button = new Efl.Ui.Button(parent);

button.Text = "Click to hide the popup";

button.ClickedEvent += (sender, args) =>
{
    popup.SetVisible(false);
};

popup.SetContent(button);

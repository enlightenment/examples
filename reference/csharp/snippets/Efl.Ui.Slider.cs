Efl.Ui.Slider slider = new Efl.Ui.Slider(parent);

slider.SetRangeLimits(0, 100);
slider.SetRangeValue(50);

// You get this event every time the slider moves
slider.ChangedEvent += (sender, args) =>
{
    Console.WriteLine("Current slider value is: " + slider.GetRangeValue());
};

// You only get this event once the slider is stable
slider.SteadyEvent += (sender, args) =>
{
    Console.WriteLine("STEADY slider value is: " + slider.GetRangeValue());
};

Efl.Ui.RadioBox radioBox = new Efl.Ui.RadioBox(parent);

for (int i = 1; i <= 3; i++)
{
    Efl.Ui.Radio radio = new Efl.Ui.Radio(radioBox);
    radio.Text = "Choice no. " + i;
    radio.SetStateValue(i);
    radioBox.Pack(radio);
}

radioBox.ValueChangedEvent += (sender, args) =>
{
    System.Console.WriteLine("RadioBox value changed! Current choice value: " + args.arg);
};

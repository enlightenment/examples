Efl.Ui.Progressbar progressbar = new Efl.Ui.Progressbar(parent);

// You can choose the range limits according to your needs.
// It can be percentage, but it can be also different value (e.g. number of files to be copied)
progressbar.SetRangeLimits(0.0, 100.0);

// Setting initial progress value
progressbar.RangeValue = 0;

// When progress is made you should modify the RangeValue:
progressbar.RangeValue = 33;

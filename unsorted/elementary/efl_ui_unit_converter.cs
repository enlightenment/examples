using System;

public class Example
{

    private static double KMS_PER_MILE = 1.609344;

    private static double KmsToMiles(double kms)
    {
        return kms / KMS_PER_MILE;
    }
    private static double MilesToKms(double miles)
    {
        return miles * KMS_PER_MILE;
    }

    private static void ShowErrorPopup(Efl.Ui.Win win, string message)
    {
        Efl.Ui.Popup_Alert popup = new Efl.Ui.Popup_Alert(win);
        Efl.Ui.Text popup_text = new Efl.Ui.Text(popup);
        popup_text.Text = $"Error: {message}";
        popup.SetContent(popup_text);
        popup.SetVisible(true);
        popup.SetButton(Efl.Ui.Popup_Alert_Button.Positive, "Ok");
        popup.Size = new Eina.Size2D(150, 30);
        popup.ButtonClickedEvent += (object sender, Efl.Ui.Popup_Alert.ButtonClickedEventArgs e) => {
            popup.Del()
        };
    }

#if WIN32 // Passed to the C# compiler with -define:WIN32
    // Mono on Windows by default uses multi-thread apartments for COM stuff while
    // OLE - used by ecore win32 DnD requires single threading for COM.
    [STAThreadAttribute()]
#endif
    public static void Main() {
        int W = 120;
        int H = 30;
        Eina.Size2D size = new Eina.Size2D(W, H);

        Efl.All.Init(Efl.Components.Ui);

        Efl.Ui.Win win = new Efl.Ui.Win(null);
        win.Text = "C# Unit Converter";
        win.Autohide = true;

        Efl.Ui.Box_Flow box = new Efl.Ui.Box_Flow(win);
        box.Orientation = Efl.Ui.LayoutOrientation.Horizontal;

        Efl.Ui.Box_Flow miles_box = new Efl.Ui.Box_Flow(box);
        miles_box.Orientation = Efl.Ui.LayoutOrientation.Down;

        box.DoPack(miles_box);

        Efl.Ui.Text miles_label = new Efl.Ui.Text(miles_box);
        miles_label.Text = "Miles:";
        miles_label.Size = size;
        miles_label.SetVisible(true);

        Efl.Ui.Text_Editable miles_input = new Efl.Ui.Text_Editable(miles_box);
        miles_input.Text = "";
        miles_input.SetScrollable(true);
        miles_input.Size = size;
        miles_input.SetVisible(true);

        Efl.Ui.Button miles_button = new Efl.Ui.Button(miles_box);
        miles_button.Text = "To Km";
        miles_button.Size = size;
        miles_button.SetVisible(true);

        miles_box.DoPack(miles_label);
        miles_box.DoPack(miles_input);
        miles_box.DoPack(miles_button);


        Efl.Ui.Box_Flow kms_box = new Efl.Ui.Box_Flow(box);
        kms_box.Orientation = Efl.Ui.LayoutOrientation.Down;

        box.DoPack(kms_box);

        Efl.Ui.Text kms_label = new Efl.Ui.Text(kms_box);
        kms_label.Text = "Kilometers:";
        kms_label.Size = size;
        kms_label.SetVisible(true);

        Efl.Ui.Text_Editable kms_input = new Efl.Ui.Text_Editable(kms_box);
        kms_input.Text = "";
        kms_input.SetScrollable(true);
        kms_input.Size = size;
        kms_input.SetVisible(true);

        Efl.Ui.Button kms_button = new Efl.Ui.Button(kms_box);
        kms_button.Text = "To Miles";
        kms_button.Size = size;
        kms_button.SetVisible(true);

        kms_box.DoPack(kms_label);
        kms_box.DoPack(kms_input);
        kms_box.DoPack(kms_button);

        kms_button.ClickedEvent += (object sender, EventArgs e) => {
            try
            {
                string text = kms_input.Text;
                Console.WriteLine("Text is [{0}]", text);
                double val = double.Parse(text);
                miles_input.Text = String.Format("{0:f3}", KmsToMiles(val));
                kms_input.SetFocus(true);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Exception {0} caught", ex);
                ShowErrorPopup(win, "Invalid number");
            }
        };

        miles_button.ClickedEvent += (object sender, EventArgs e) => {
            try
            {
                string text = miles_input.Text;
                Console.WriteLine("Text is [{0}]", text);
                double val = double.Parse(text);
                kms_input.Text = String.Format("{0:f3}", MilesToKms(val));
                miles_input.SetFocus(true);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Exception {0} cautght", ex);
                ShowErrorPopup(win, "Invalid number");
            }
        };

        kms_box.SetVisible(true);
        miles_box.SetVisible(true);

        box.SetPosition(new Eina.Position2D(20, 30));
        box.SetVisible(true);

        win.SetPosition(new Eina.Position2D(200, 200));

        win.Size = new Eina.Size2D(400, 120);
        win.SetVisible(true);

        Efl.Ui.Config.Run();

        Efl.All.Shutdown();
    }

}

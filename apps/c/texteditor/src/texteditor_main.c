#define EFL_BETA_API_SUPPORT 1

#include <Elementary.h>
#include <Efl_Ui.h>

Efl_Ui_Textbox *_editor;
Efl_Ui_Button *_toolbar_new;

Eina_Bool _edited = EINA_FALSE;

static void _gui_toolbar_refresh(void);

static void
_gui_new_clicked_cb(void *data EINA_UNUSED, const Efl_Event *event EINA_UNUSED)
{
   efl_text_set(_editor, "");
   _edited = EINA_FALSE;
   _gui_toolbar_refresh();
}

static void
_gui_quit_cb(void *data EINA_UNUSED, const Efl_Event *event EINA_UNUSED)
{
   efl_exit(0);
}

static void
_editor_changed_cb(void *data EINA_UNUSED, const Efl_Event *event EINA_UNUSED)
{
   _edited = EINA_TRUE;
   _gui_toolbar_refresh();
}

static Efl_Ui_Button *
_gui_toolbar_button_add(Efl_Ui_Box *toolbar, const char *name,
                        const char *icon_name, Efl_Event_Cb func)
{
   Efl_Ui_Button *button;

   button = efl_add(EFL_UI_BUTTON_CLASS, toolbar,
                    efl_text_set(efl_added, name),
                    efl_pack(toolbar, efl_added),
                    efl_event_callback_add(efl_added, EFL_INPUT_EVENT_CLICKED,
                                           func, efl_added));

   efl_add(EFL_UI_IMAGE_CLASS, toolbar,
           efl_ui_image_icon_set(efl_added, icon_name),
           efl_content_set(button, efl_added));

   return button;
}

static void
_gui_toolbar_setup(Efl_Ui_Box *parent)
{
   Efl_Ui_Box *bar;

   bar = efl_add(EFL_UI_BOX_CLASS, parent,
                 efl_pack(parent, efl_added),
                 efl_gfx_hint_weight_set(efl_added, 1, 0),
                 efl_ui_layout_orientation_set(efl_added, EFL_UI_LAYOUT_ORIENTATION_HORIZONTAL));

   _toolbar_new = _gui_toolbar_button_add(bar, "New", "document-new", _gui_new_clicked_cb);

   // spacer box
   efl_add(EFL_UI_BOX_CLASS, parent,
           efl_pack(bar, efl_added),
           efl_gfx_hint_weight_set(efl_added, 10, 0));
   _gui_toolbar_button_add(bar, "Quit", "application-exit", _gui_quit_cb);

   _gui_toolbar_refresh();
}

static void
_gui_toolbar_refresh(void)
{
   efl_ui_widget_disabled_set(_toolbar_new, !_edited);
}

EFL_CALLBACKS_ARRAY_DEFINE(_editor_callbacks,
                           { EFL_UI_TEXTBOX_EVENT_CHANGED, _editor_changed_cb },
                           { EFL_TEXT_INTERACTIVE_EVENT_CHANGED_USER, _editor_changed_cb });

static void
_gui_setup()
{
   Eo *win, *box;

   win = efl_add(EFL_UI_WIN_CLASS, efl_main_loop_get(),
                 efl_ui_win_type_set(efl_added, EFL_UI_WIN_TYPE_BASIC),
                 efl_text_set(efl_added, "Text Editor"),
                 efl_ui_win_autodel_set(efl_added, EINA_TRUE));

   // when the user clicks "close" on a window there is a request to delete
   efl_event_callback_add(win, EFL_UI_WIN_EVENT_DELETE_REQUEST, _gui_quit_cb, NULL);

   box = efl_add(EFL_UI_BOX_CLASS, win,
                 efl_content_set(win, efl_added),
                 efl_gfx_hint_size_min_set(efl_added, EINA_SIZE2D(360, 240)));

   _gui_toolbar_setup(box);

   _editor = efl_add(EFL_UI_TEXTBOX_CLASS, box,
                     efl_text_multiline_set(efl_added, EINA_TRUE),
                     efl_text_interactive_editable_set(efl_added, EINA_TRUE),
                     efl_event_callback_array_add(efl_added, _editor_callbacks(), NULL),
                     efl_pack(box, efl_added));
   efl_ui_textbox_scrollable_set(_editor, EINA_TRUE);
   efl_text_font_family_set(_editor, "Mono");
   efl_text_font_size_set(_editor, 14);
}

EAPI_MAIN void
efl_main(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   _gui_setup();
}
EFL_MAIN()


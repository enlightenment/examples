#define EFL_BETA_API_SUPPORT 1

#include <Efl_Ui.h>

#include "life_private.h"

static void
_life_win_resize(void *data EINA_UNUSED, const Efl_Event *event)
{
   Efl_Ui_Win *win = event->object;

   life_render_layout(win);
}

static void
_life_win_quit(void *data EINA_UNUSED, const Efl_Event *event EINA_UNUSED)
{
   // quit the mainloop
   efl_exit(0);
}

static void
_life_win_touch(void *data EINA_UNUSED, const Efl_Event *event)
{
   int cellx, celly, i;
   Efl_Input_Pointer *ev;
   Efl_Ui_Win *win;
   Eina_Position2D position;

   ev = event->info;
   win = event->object;

   position = efl_input_pointer_position_get(ev);
   life_render_cell_for_coords(win, position, &cellx, &celly);

   i = life_render_index_for_position(cellx, celly);
   life_board[i] = !life_board[i];
   life_render_cell(win, cellx, celly);
}

static void
_life_win_key_down(void *data EINA_UNUSED, const Efl_Event *event)
{
   Efl_Input_Key *ev;
   Efl_Ui_Win *win;

   ev = event->info;
   win = event->object;

   if (!strcmp(efl_input_key_sym_get(ev), "space"))
     life_board_pause_toggle(win);
}

static Efl_Ui_Win *
_life_win_setup(void)
{
   Efl_Ui_Win *win;
   int w;
   int h;

   win = efl_add(EFL_UI_WIN_CLASS, efl_main_loop_get(),
                 efl_ui_win_type_set(efl_added, EFL_UI_WIN_TYPE_BASIC),
                 efl_text_set(efl_added, "EFL Life"),
                 efl_ui_win_autodel_set(efl_added, EINA_TRUE));
   if (!win) return NULL;

   // when the user clicks "close" on a window there is a request to delete
   efl_event_callback_add(win, EFL_UI_WIN_EVENT_DELETE_REQUEST, _life_win_quit, NULL);

   w = 10 * LIFE_BOARD_WIDTH * efl_gfx_entity_scale_get(win);
   h = 10 * LIFE_BOARD_HEIGHT * efl_gfx_entity_scale_get(win);

   life_board_init();
   life_render_init(win);
   life_render_refresh(win);

   efl_event_callback_add(win, EFL_GFX_ENTITY_EVENT_SIZE_CHANGED, _life_win_resize, NULL);
   efl_event_callback_add(win, EFL_EVENT_POINTER_DOWN, _life_win_touch, NULL);
   efl_event_callback_add(win, EFL_EVENT_KEY_DOWN, _life_win_key_down, NULL);

   efl_gfx_entity_size_set(win, EINA_SIZE2D(w, h));

   return win;
}

EAPI_MAIN void
efl_main(void *data EINA_UNUSED, const Efl_Event *ev EINA_UNUSED)
{
   Efl_Ui_Win *win;

   if (!(win = _life_win_setup()))
     efl_exit(1);

   life_board_run(win);
}
EFL_MAIN()

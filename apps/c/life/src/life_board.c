#define EFL_BETA_API_SUPPORT 1

#include <Efl_Ui.h>

#include "life_private.h"

static int gen = 0;

int *life_board, *life_board_prev;
static int *_life_board_1, *_life_board_2;
static Efl_Loop_Timer *_life_timer;

static void
_life_tick(void *data, const Efl_Event *event EINA_UNUSED)
{
   Efl_Ui_Win *win = data;

   life_board_nextgen();
   life_render_refresh(win);
}

static void
_life_cell_on(int x, int y)
{
   _life_board_1[life_render_index_for_position(x, y)] = 1;
}

static void
_life_board_setup()
{
   // glide
   _life_cell_on(16, 1);
   _life_cell_on(17, 2);
   _life_cell_on(18, 2);
   _life_cell_on(16, 3);
   _life_cell_on(17, 3);

   // oscilate
   _life_cell_on(22, 15);
   _life_cell_on(23, 15);
   _life_cell_on(24, 15);

   // block
   _life_cell_on(32, 15);
   _life_cell_on(33, 15);
   _life_cell_on(32, 16);
   _life_cell_on(33, 16);
}

void
life_board_init()
{
   _life_board_1 = calloc(1, sizeof(int) * LIFE_BOARD_WIDTH * LIFE_BOARD_HEIGHT);
   _life_board_2 = calloc(1, sizeof(int) * LIFE_BOARD_WIDTH * LIFE_BOARD_HEIGHT);

   _life_board_setup();

   life_board = _life_board_1;
   life_board_prev = _life_board_2;
}

void
life_board_run(Efl_Ui_Win *win)
{
   _life_timer = efl_add(EFL_LOOP_TIMER_CLASS, efl_main_loop_get(),
                         efl_loop_timer_interval_set(efl_added, 0.1));

   efl_event_callback_add(_life_timer, EFL_LOOP_TIMER_EVENT_TIMER_TICK, _life_tick, win);
}

int
life_board_sum_around(int x, int y)
{
   int sum = 0;
   int i, max;

   max = LIFE_BOARD_WIDTH * LIFE_BOARD_HEIGHT;

   i = life_render_index_for_position(x - 1, (y - 1));
   if (i >= 0)
     sum += life_board[i];
   i++;
   if (i >= 0)
     sum += life_board[i];
   i++;
   if (i >= 0)
     sum += life_board[i];

   i = life_render_index_for_position(x - 1, y);
   if (i >= 0)
     sum += life_board[i];
   i += 2;
   if (i < max)
     sum += life_board[i];

   i = life_render_index_for_position(x - 1, (y + 1));
   if (i < max)
     sum += life_board[i];
   i++;
   if (i < max)
     sum += life_board[i];
   i++;
   if (i < max)
     sum += life_board[i];

   return sum;
}

void
life_board_nextgen()
{
   int *work;
   int x, y, i, n;
   gen++;

   if (life_board == _life_board_1)
     work = _life_board_2;
   else
     work = _life_board_1;

   for (y = 0; y < LIFE_BOARD_HEIGHT; y++)
     for (x = 0; x < LIFE_BOARD_WIDTH; x++)
       {
          i = life_render_index_for_position(x, y);

          n = life_board_sum_around(x, y);
          if (life_board[i])
            {
               if (n > 3 || n < 2)
                 work[i] = 0;
               else
                 work[i] = 1;
            }
          else
            {
               if (n == 3)
                 work[i] = 1;
               else
                 work[i] = 0;
            }
       }

   life_board_prev = life_board;
   life_board = work;
}

void
life_board_pause_toggle(Efl_Ui_Win *win)
{
   if (_life_timer)
     {
        efl_del(_life_timer);
        _life_timer = NULL;
     }
   else
     {
        life_board_run(win);
     }
}


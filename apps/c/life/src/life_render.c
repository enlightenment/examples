#define EFL_BETA_API_SUPPORT 1

#include <Efl_Ui.h>

#include "life_private.h"

Efl_Canvas_Rectangle **_life_cells;

void
life_render_init(Efl_Ui_Win *win)
{
   int x, y;

   _life_cells = calloc(1, sizeof(Efl_Canvas_Rectangle *) * LIFE_BOARD_WIDTH * LIFE_BOARD_HEIGHT);

   for (y = 0; y < LIFE_BOARD_HEIGHT; y++)
     for (x = 0; x < LIFE_BOARD_WIDTH; x++)
       efl_add(EFL_CANVAS_RECTANGLE_CLASS, win,
               _life_cells[life_render_index_for_position(x, y)] = efl_added);

   life_render_layout(win);
}

void
life_render_cell_for_coords(Efl_Ui_Win *win, Eina_Position2D coord,
                            int *x, int *y)
{
   Eina_Size2D size;

   size = efl_gfx_entity_size_get(win);

   if (x)
     *x = coord.x / ((double) size.w / LIFE_BOARD_WIDTH);
   if (y)
     *y = coord.y / ((double) size.h / LIFE_BOARD_HEIGHT);
}

int
life_render_index_for_position(int x, int y)
{
   return y * LIFE_BOARD_WIDTH + x;
}

void
life_render_layout(Efl_Ui_Win *win)
{
   Eina_Size2D size;
   double cw, ch;
   Efl_Canvas_Rectangle *rect;
   int x, y;

   size = efl_gfx_entity_size_get(win);
   cw = (double) size.w / LIFE_BOARD_WIDTH;
   ch = (double) size.h / LIFE_BOARD_HEIGHT;

   for (y = 0; y < LIFE_BOARD_HEIGHT; y++)
     for (x = 0; x < LIFE_BOARD_WIDTH; x++)
       {
          rect = _life_cells[life_render_index_for_position(x, y)];

          // the little +1 here will avoid tearing as we layout non-multiple sizes
          efl_gfx_entity_size_set(rect, EINA_SIZE2D(cw + 1, ch + 1));
          efl_gfx_entity_position_set(rect, EINA_POSITION2D(x * cw, y * ch));
       }
}

void
life_render_cell(Efl_Ui_Win *win EINA_UNUSED, int x, int y)
{
   Efl_Canvas_Rectangle *rect;
   int i;

   i = life_render_index_for_position(x, y);
   rect = _life_cells[i];

   if (life_board[i])
     efl_gfx_color_set(rect, 0, 0, 0, 255);
   else
     efl_gfx_color_set(rect, 255, 255, 255, 255);
}

void
life_render_refresh(Efl_Ui_Win *win EINA_UNUSED)
{
   int x, y;

   for (y = 0; y < LIFE_BOARD_HEIGHT; y++)
     for (x = 0; x < LIFE_BOARD_WIDTH; x++)
       life_render_cell(win, x, y);
}


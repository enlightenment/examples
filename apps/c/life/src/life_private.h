#ifndef LIFE_PRIVATE_H_
# define LIFE_PRIVATE_H_

#include <Efl_Ui.h>

#define LIFE_BOARD_WIDTH 47
#define LIFE_BOARD_HEIGHT 31

extern int *life_board, *life_board_prev;

void life_board_init(void);

void life_board_nextgen(void);
void life_board_run(Efl_Ui_Win *win);
void life_board_pause_toggle(Efl_Ui_Win *win);

void life_render_init(Efl_Ui_Win *win);
void life_render_cell_for_coords(Efl_Ui_Win *win, Eina_Position2D coord, int *x, int *y);
int life_render_index_for_position(int x, int y);
void life_render_layout(Efl_Ui_Win *win);
void life_render_cell(Efl_Ui_Win *win, int x, int y);
void life_render_refresh(Efl_Ui_Win *win);

#endif

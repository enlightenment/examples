/* Simple text editor with a main text box and a toolbar on top:
   +vbox----------------------------------------+
   | +hbox------------------------------------+ |
   | | +btn-+ +btn-+ +btn-+ +box-----+ +btn-+ | |
   | | |NEW | |SAVE| |LOAD| | spacer | |QUIT| | |
   | | +----+ +----+ +----+ +--------+ +----+ | |
   | +----------------------------------------+ |
   | +text------------------------------------+ |
   | |                                        | |
   | |                                        | |
   | |         Main text box                  | |
   | |                                        | |
   | |                                        | |
   | +----------------------------------------+ |
   +--------------------------------------------+
*/

using System;

public class TextEditor : Efl.Csharp.Application
{
    private Efl.Ui.Win win;                  // The main window
    private Efl.Ui.Textbox editorTextBox;    // The main text entry
    private Efl.Ui.Button toolbarButtonNew;  // The "New" button in the toolbar
    private Efl.Ui.Button toolbarButtonSave; // The "Save" button in the toolbar
    private Efl.Ui.Button toolbarButtonLoad; // The "Load" button in the toolbar

    private bool edited = false;             // Document was edited since last save

    // File to load and save is fixed since we do not use a file selection dialog
    private readonly string filename = System.IO.Path.Combine(System.IO.Path.GetTempPath(),
                                                              "texteditor_example.txt");

    // Quits the application
    private void GUIQuitCb(object sender, Efl.Gfx.EntityVisibilityChangedEventArgs ea)
    {
        if (ea.Arg == false)
            Efl.App.AppMain.Quit(0);
    }

    // Enables or disables buttons on the toolbar as required
    private void GUIToolbarRefresh()
    {
        // "New" is enabled if there is text in the text box
        toolbarButtonNew.Disabled = string.IsNullOrEmpty(editorTextBox.Text);
        // "Save" is enabled if the text has been modified since last save or load
        toolbarButtonSave.Disabled = !edited;
        // "Load" is enabled if there is a file to load
        toolbarButtonLoad.Disabled = !System.IO.File.Exists(filename);
    }

    // Called when the text in the editor has changed
    private void EditorChangedCb(object sender, EventArgs ea)
    {
        edited = true;
        GUIToolbarRefresh();
    }

    // Shows a modal message popup with an "OK" button
    private void ShowMessage(string message)
    {
        var popup = new Efl.Ui.AlertPopup (win);
        popup.ScrollableText = message;
        popup.HintSizeMax = new Eina.Size2D(200, 200);
        popup.SetButton(Efl.Ui.AlertPopupButton.Positive, "OK", null);
        popup.ButtonClickedEvent +=
            (object sender, Efl.Ui.AlertPopupButtonClickedEventArgs ea) => {
          // Dismiss popup when the button is clicked
          ((Efl.Ui.AlertPopup)sender).Del();
        };
    }

    // Adds a button to the toolbar, with the given text, icon and click event handler
    private Efl.Ui.Button GUIToolbarButtonAdd(Efl.Ui.Box toolbar, string name,
                                              string iconName, EventHandler<Efl.Input.ClickableClickedEventArgs> func)
    {
        var button = new Efl.Ui.Button(toolbar);
        button.Text = name;
        button.ClickedEvent += func;
        button.HintWeight = (0, 1);

        // Set the content of the button, which is an image
        var image = new Efl.Ui.Image(toolbar);
        image.SetIcon(iconName);
        button.SetContent(image);

        toolbar.Pack(button);
        return button;
    }

    // Creates a new toolbar, with all its buttons
    private void GUIToolbarSetup(Efl.Ui.Box parent)
    {
        // Create a horizontal box container for the buttons
        var bar = new Efl.Ui.Box(parent);
        // 0 vertical weight means that the toolbar will have the minimum height
        // to accommodate all its buttons and not a pixel more. The rest of the
        // space will be given to the other object in the parent container.
        bar.HintWeight = (1, 0);
        bar.Orientation = Efl.Ui.LayoutOrientation.Horizontal;
        parent.Pack(bar);

        // "New" button
        toolbarButtonNew = GUIToolbarButtonAdd(bar, "New", "document-new",
          (object sender, Efl.Input.ClickableClickedEventArgs ea) => {
              // When this button is clicked, remove content and refresh toolbar
              editorTextBox.Text = "";
              GUIToolbarRefresh();
          });

        // "Save" button
        toolbarButtonSave = GUIToolbarButtonAdd(bar, "Save", "document-save",
          (object sender, Efl.Input.ClickableClickedEventArgs ea) => {
              // When this button is clicked, try to save content and refresh toolbar
              try {
                System.IO.File.WriteAllText(filename, editorTextBox.Text);
                edited = false;
                GUIToolbarRefresh();
                ShowMessage("Saved!");
              } catch (Exception e) {
                // If something fails, show the error message
                ShowMessage(e.Message);
              }
          });

        // "Load" button
        toolbarButtonLoad = GUIToolbarButtonAdd(bar, "Load", "document-open",
          (object sender, Efl.Input.ClickableClickedEventArgs ea) => {
              // When this button is clicked, try to load content and refresh toolbar
              try {
                editorTextBox.Text = System.IO.File.ReadAllText(filename);
                edited = false;
                GUIToolbarRefresh();
                ShowMessage("Loaded!");
              } catch (Exception e) {
                // If something fails, show the error message
                ShowMessage(e.Message);
              }
          });

        // Spacer box to use all available space not required by buttons
        // (It has a default horizontal weight of 1, whereas all buttons have
        // a horizontal weight of 0).
        // As a result, it pushes the "Quit" button to the right margin and
        // the rest to the left.
        Efl.Ui.Box box = new Efl.Ui.Box(parent);
        bar.Pack(box);

        // "Quit" button
        GUIToolbarButtonAdd(bar, "Quit", "application-exit", (object sender, Efl.Input.ClickableClickedEventArgs e) => { Efl.Ui.Config.Exit(); } );
    }

    // Builds the user interface for the text editor
    protected override void OnInitialize(string[] args)
    {
        // Create a window and initialize it
        win = new Efl.Ui.Win(parent: Efl.App.AppMain);
        win.Text = "Text Editor";
        win.Autohide = true;
        win.VisibilityChangedEvent += GUIQuitCb;

        // Create a vertical box container
        Efl.Ui.Box box = new Efl.Ui.Box(win);
        win.SetContent(box);

        // Create the toolbar and add it to the box
        GUIToolbarSetup(box);

        // Create the main text entry
        editorTextBox = new Efl.Ui.Textbox(box);
        editorTextBox.FontFamily = "Mono";
        editorTextBox.FontSize = 14;
        editorTextBox.Multiline = true;
        editorTextBox.Editable = true;
        editorTextBox.Scrollable = true;
        editorTextBox.HintSizeMin = new Eina.Size2D(360, 240);
        editorTextBox.ChangedEvent += EditorChangedCb;
        editorTextBox.ChangedUserEvent += EditorChangedCb;
        box.Pack(editorTextBox);

        // Initial refresh of the toolbar buttons
        GUIToolbarRefresh();
    }
}

public class Example
{
#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        TextEditor editor = new TextEditor();
        editor.Launch();
    }
}


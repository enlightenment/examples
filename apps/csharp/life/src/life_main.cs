using System;

public class LifeWindow : Efl.Csharp.Application
{
    private LifeBoard lifeBoard;
    private LifeRender lifeRender;
    private Efl.Ui.Win win;

    void ResizeEvent(object sender, EventArgs ev)
    {
        lifeRender.RenderLayout((Efl.Ui.Win)sender);
    }

    void QuitEvent(object sender, Efl.Gfx.EntityVisibilityChangedEventArgs ev)
    {
        // quit the mainloop
        if (ev.Arg == false)
            Efl.App.AppMain.Quit(0);
    }

    void TouchEvent(object sender, Efl.Input.InterfacePointerDownEventArgs ev)
    {
        int cellx, celly;
        var position = ev.Arg.Position;
        lifeRender.CellForCoords(win, position, out cellx, out celly);

        int i = LifeBoard.IndexForPosition(cellx, celly);
        lifeBoard.Cells[i] = !lifeBoard.Cells[i];
        lifeRender.RenderCell(win, cellx, celly);
    }

    void KeyDownEvent(object sender, Efl.Input.InterfaceKeyDownEventArgs ev)
    {
        if (ev.Arg.KeySym == "space")
            lifeBoard.TogglePause(win);
    }

    protected override void OnInitialize(string[] args)
    {
        win = new Efl.Ui.Win(parent: null, winName: "Life", winType: Efl.Ui.WinType.Basic);
        win.Text = "EFL Life";
        win.Autohide = true;

        // when the user clicks "close" on a window there is a request to hide
        ((Efl.Gfx.IEntity)win).VisibilityChangedEvent += QuitEvent;

        lifeBoard = new LifeBoard();
        lifeRender = new LifeRender(win, lifeBoard);
        lifeRender.Refresh(win);

        ((Efl.Gfx.IEntity)win).SizeChangedEvent += ResizeEvent;
        ((Efl.Input.IInterface)win).PointerDownEvent += TouchEvent;
        ((Efl.Input.IInterface)win).KeyDownEvent += KeyDownEvent;

        win.Size = new Eina.Size2D((int)(10 * LifeBoard.Width * win.Scale),
                                   (int)(10 * LifeBoard.Height * win.Scale));

        lifeBoard.Run(win);
    }

    protected override void OnPause() {
        if (win != null) {
            lifeBoard.TogglePause(win);
        }
    }

    protected override void OnResume() {
        if (win != null) {
            lifeBoard.TogglePause(win);
        }
    }

    protected override void OnTerminate() {
        Console.WriteLine("Goodbye.");
    }
}

public class Example
{
    public static void Main()
    {
        var lifeWin = new LifeWindow();

        lifeWin.Launch();
    }
}

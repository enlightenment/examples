using System;

public class LifeRender
{
    private Efl.Canvas.Rectangle[] lifeCells;
    private LifeBoard lifeBoard;

    public LifeRender(Efl.Ui.Win win, LifeBoard board)
    {
        lifeBoard = board;
        lifeBoard.SetRender(this);

        lifeCells = new Efl.Canvas.Rectangle[LifeBoard.Height * LifeBoard.Width];

        for (int y = 0; y < LifeBoard.Height; ++y)
            for (int x = 0; x < LifeBoard.Width; ++x)
                lifeCells[LifeBoard.IndexForPosition(x, y)] = new Efl.Canvas.Rectangle(win);

        RenderLayout(win);
    }

    public void CellForCoords(Efl.Ui.Win win, Eina.Position2D coord, out int x, out int y)
    {
        Eina.Size2D size = win.Size;

        x = coord.X * LifeBoard.Width / size.W;
        y = coord.Y * LifeBoard.Height / size.H;
    }

    public void RenderLayout(Efl.Ui.Win win)
    {
        Eina.Size2D size = win.Size;
        double cw = (double) size.W / LifeBoard.Width;
        double ch = (double) size.H / LifeBoard.Height;

        for (int y = 0; y < LifeBoard.Height; ++y)
            for (int x = 0; x < LifeBoard.Width; ++x)
            {
                var rect = lifeCells[LifeBoard.IndexForPosition(x, y)];

                // the little +1 here will avoid tearing as we layout non-multiple sizes
                rect.Size = new Eina.Size2D((int)(cw + 1), (int)(ch + 1));

                rect.Position = new Eina.Position2D((int)(x * cw), (int)(y * ch));
            }
    }

    public void RenderCell(Efl.Ui.Win win, int x, int y)
    {
        int i = LifeBoard.IndexForPosition(x, y);
        var rect = lifeCells[i];

        if (lifeBoard.Cells[i])
            rect.Color = (0, 0, 0, 255);
        else
            rect.Color = (255, 255, 255, 255);
    }

    public void Refresh(Efl.Ui.Win win)
    {
        for (int y = 0; y < LifeBoard.Height; ++y)
            for (int x = 0; x < LifeBoard.Width; ++x)
                RenderCell(win, x, y);
    }
}

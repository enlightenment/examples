/* Simple calculator using an Efl.Ui.Table to place the buttons
*/
using System;

public class Calculator : Efl.Csharp.Application
{
    private Efl.Ui.Textbox screen;      // Text widget showing current value
    private int prevValue = 0;          // Value introduced before an operation (first operand)
    private int currValue = 0;          // Value currently being introduced (second operand)
    private char operation = '=';       // Last operation button pressed
    private bool mustOverwrite = false; // Whether next number must be appended to current input
                                        // or overwrite it

    // Quits the application
    private void GUIQuitCb(object sender, Efl.Gfx.EntityVisibilityChangedEventArgs ea)
    {
        if (ea.Arg == false)
            Efl.App.AppMain.Quit(0);
    }

    // Performs "operation" on "currValue" and "prevValue" and leaves result in "currValue"
    private void Operate()
    {
       switch (operation)
       {
          case '+':
             currValue += prevValue;
             break;
          case '-':
             currValue = prevValue - currValue;
             break;
          case '*':
             currValue *= prevValue;
             break;
          case '/':
             currValue = prevValue / currValue;
             break;
          default:
             break;
       }
    }

    // Called every time a button is pressed
    private void ButtonPressedCb(char button)
    {
        // If it is a number, append it to current input (or replace it)
        if (button >= '0' && button <= '9')
          {
             if (mustOverwrite)
               {
                  screen.Text = "";
                  mustOverwrite = false;
               }
             screen.Text = screen.Text + button.ToString();
          }
        else
          {
             switch (button)
             {
                case 'C':
                   // Clear current input
                   screen.Text = "0";
                   break;
                case '+':
                case '-':
                case '*':
                case '/':
                case '=':
                   // If there was a pending operation, perform it
                   if (operation != '=')
                     {
                        Operate();
                        screen.Text = currValue.ToString();
                     }
                   // Store this operation
                   operation = button;
                   mustOverwrite = true;
                   prevValue = currValue;
                   break;
                default:
                   break;
             }
          }
    }

    // Called every time the content of the screen changes
    // We use it to sanitize input (remove heading zeros, for example)
    // This makes more sense when the Text widget is editable, since the user
    // is free to type anything.
    private void ScreenChangedCb(object sender, EventArgs ea)
    {
       string text = "";
       string str = screen.Text;
       int d;
       if (str == "" || str == "-")
         {
            text = "0";
         }
       else
         {
            try
              {
                 d = Convert.ToInt32(str);
                 text = d.ToString();
                 currValue = d;
              }
            catch {}
         }
       if (text != str) screen.Text = text;
    }

    // Creates an Efl.Ui.Button and positions it in the given position inside the table
    // The button text is colored with "r, g, b"
    // "text" is what is drawn on the button, which might be a multi-byte unicode string.
    // "command" is a single-char id for the button.
    private void AddButton(Efl.Ui.Table table, string text, char command, int posx, int posy, int r, int g, int b)
    {
       var button = new Efl.Ui.Button(table);
       table.PackTable(button, posx, posy, 1, 1);
       button.ClickedEvent += (object sender, Efl.Input.ClickableClickedEventArgs ea) => {
          ButtonPressedCb(command);
       };
       // Buttons can only have simple text (no font, styles or markup) but can swallow
       // any other object we want.
       // Therefore we create a more complex Efl.Ui.Text object and use it as content for the button.
       var label = new Efl.Ui.Textbox(table);
       label.Editable = false;
       label.TextHorizontalAlign = 0.5;
       label.TextVerticalAlign = 0.5;
       label.Color = (r, g, b, 255);
       label.Text = text;
       label.FontFamily = "Sans";
       label.FontSize = 36;
       button.Content = label;
    }

    // Called on start up. We use it to create the UI.
    protected override void OnInitialize(string[] args)
    {
       // The window
       var win = new Efl.Ui.Win(Efl.App.AppMain);
       win.Text = "EFL Calculator";
       win.Autohide = true;
       win.VisibilityChangedEvent += GUIQuitCb;

       // The table is the main layout
       var table = new Efl.Ui.Table(win);
       win.Content = table;
       table.TableSize = (4, 5);
       table.HintSizeMin = new Eina.Size2D(300, 400);

       // Create all buttons using the AddButton helper
       AddButton(table, "1", '1', 0, 3, 255, 255, 255);
       AddButton(table, "2", '2', 1, 3, 255, 255, 255);
       AddButton(table, "3", '3', 2, 3, 255, 255, 255);
       AddButton(table, "4", '4', 0, 2, 255, 255, 255);
       AddButton(table, "5", '5', 1, 2, 255, 255, 255);
       AddButton(table, "6", '6', 2, 2, 255, 255, 255);
       AddButton(table, "7", '7', 0, 1, 255, 255, 255);
       AddButton(table, "8", '8', 1, 1, 255, 255, 255);
       AddButton(table, "9", '9', 2, 1, 255, 255, 255);
       AddButton(table, "0", '0', 1, 4, 255, 255, 255);
       AddButton(table, "+", '+', 3, 1, 128, 128, 128);
       AddButton(table, "−", '-', 3, 2, 128, 128, 128);
       AddButton(table, "×", '*', 3, 3, 128, 128, 128);
       AddButton(table, "÷", '/', 3, 4, 128, 128, 128);
       AddButton(table, "=", '=', 2, 4, 128, 128, 128);
       AddButton(table, "C", 'C', 0, 4,   0,   0,   0);

       // Create a big Efl.Ui.Text screen to display the current input
       screen = new Efl.Ui.Textbox(table);
       screen.Text = "0";
       screen.Multiline = false;
       screen.Editable = false;
       screen.SelectionAllowed = false;
       screen.TextHorizontalAlign = 0.9;
       screen.TextVerticalAlign = 0.5;
       screen.TextEffectType = Efl.TextStyleEffectType.Glow;
       screen.TextGlowColor = (128, 128, 128, 128);
       screen.FontFamily = "Sans";
       screen.FontSize = 48;
       screen.ChangedEvent += ScreenChangedCb;
       table.PackTable(screen, 0, 0, 4, 1);
    }
}

public class Example
{
#if WIN32
    [STAThreadAttribute()]
#endif
    public static void Main()
    {
        var calculator = new Calculator();
        calculator.Launch();
    }
}


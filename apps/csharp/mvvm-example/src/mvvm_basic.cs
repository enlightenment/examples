using System;

class WeatherStation
{
    public String Nick { get; set; }
    public float Temperature { get; set; }

    public static Efl.UserModel<WeatherStation> CreateModel(Efl.Loop loop)
    {
        Efl.UserModel<WeatherStation> stations = new Efl.UserModel<WeatherStation>(loop);
        stations.Add (new WeatherStation{ Nick="FLN", Temperature=20 });
        stations.Add (new WeatherStation{ Nick="SAO", Temperature=25 });
        stations.Add (new WeatherStation{ Nick="RIO", Temperature=35 });
        stations.Add (new WeatherStation{ Nick="BSB", Temperature=30 });

        return stations;
    }
}

class WeatherServer
{
}

class Application : Efl.Csharp.Application
{
    private Efl.Ui.Win win;

    protected override void OnInitialize(string[] args)
    {
        win = new Efl.Ui.Win(parent: null, winName: "MVVM Example",
                             winType: Efl.Ui.WinType.Basic);
        win.Text = "EFL Life";
        win.Autohide = true;

        win.VisibilityChangedEvent += QuitEvt;

        var factory = new Efl.Ui.ItemFactory<Efl.Ui.ListDefaultItem>(win);
        // Text property is temporarily excluded from the extension method generation
        // due to conflicts with the text classes.
        factory.BindProperty("text", "Nick");

        var model = WeatherStation.CreateModel(Efl.App.AppMain);

        var manager = new Efl.Ui.PositionManager.List(win);
        var list = new Efl.Ui.CollectionView(win);

        list.PositionManager = manager;
        list.Model = model;
        list.Factory = factory;

        win.SetContent(list);
        win.Size = new Eina.Size2D(640, 480);
        win.Visible = true;
    }

    void QuitEvt(object sender, Efl.Gfx.EntityVisibilityChangedEventArgs ev)
    {
        if (ev.Arg == false)
        {
            Efl.App.AppMain.Quit(0);
        }
    }

    public static void Main()
    {
        var app = new Application();
        app.Launch();
    }
}

#!/usr/bin/python3
import os
import subprocess
from string import Template

supported_languages = ["c"]
directories = ["apps", "reference", "tutorial"] # "examples", "legacy-examples"
goals = []
subprojects = []

if subprocess.call("pkg-config --exists efl-mono", shell=True) == 0:
  supported_languages += ["csharp"]
else:
  print("Disable c# bindings")
if subprocess.call("pkg-config --exists eina-cxx", shell=True) == 0:
  supported_languages += ["cxx"]
else:
  print("Disable c++ bindings")

class SubProjectGoal:
  def __init__(self, language, path):
    self.language = language
    self.path = path

  def verify(self):
    assert os.path.isdir(self.path)
    assert os.path.isfile(os.path.join(self.path, 'meson.build'))

  def flush(self):
    os.symlink(os.path.join('..', self.path), os.path.realpath(os.path.join('subprojects', self.link_file_name())))

  def link_file_name(self):
    return self.language+'-'+os.path.basename(self.path)

for directory in directories:
  for lang in supported_languages:
      explore_dir = os.path.join(directory, lang)
      if os.path.isdir(explore_dir):
        meson_build_file = os.path.join(explore_dir, "meson.build")
        if os.path.isfile(meson_build_file):
            goals.append(SubProjectGoal(lang, explore_dir))
        else:
          for content in os.listdir(explore_dir):
            sub = os.path.join(explore_dir, content)
            if os.path.isdir(sub):
              goals.append(SubProjectGoal(lang, sub))

if not os.path.isdir('./subprojects'):
  os.mkdir('./subprojects')
else:
  for content in os.listdir('./subprojects'):
    os.unlink(os.path.join('subprojects', content))

for subproject in goals:
  subproject.verify()
  subproject.flush()
  subprojects.append(subproject.link_file_name())

content = { 'subprojects' : '[\''+'\',\''.join(subprojects)+'\']'}

meson_in = open('meson.build.in')
meson_temp = Template(meson_in.read())
content = meson_temp.substitute(content)

if os.path.isfile('meson.build'):
  os.unlink('meson.build')

meson_out = open('meson.build', 'a')
meson_out.write(content)
